# === General settings ===============================================
NAME		:= estimator
VERSION		:= master
MUTE		:= @
SHOW		= @printf "%-16s %s\n" "$(1)" "$(2)"
#SHOW		= $(shell echo "$(1) $(2)")
ifdef VERBOSE
MUTE		:=
SHOW		:=
endif

#DEBUG		:= 1

# --- Various compiler and linker flags ------------------------------
CXX		:= g++ -c
CXXFLAGS	:= -Wall -Wextra -ansi -pedantic -g -std=c++11
CPP		:= g++
CPPFLAGS	:= -I. -Ioptions
LD		:= g++ 
LDFLAGS		:=
ifdef PROFILE
CXXFLAGS	+= -pg
LDFLAGS		+= -pg -lprofiler
endif
ifdef DEBUG
CXXFLAGS	:= $(filter-out -O3, $(CXXFLAGS))
endif
ifdef ADDRESS
CXXFLAGS	+= -fsanitize=address
LDFLAGS		+= -lasan
endif

PYTHON		:= python3

UNCER		:= Derivatives

# === The source code and documentation ==============================
STAT		:= statest/trait.hh		\
		   statest/basic_stat.hh	\
		   statest/west.hh		\
		   statest/sample.hh		\
		   statest/basic_var.hh		\
		   statest/welford_var.hh	\
		   statest/west_var.hh		\
		   statest/sample_var.hh	\
		   statest/basic_cov.hh		\
		   statest/welford_cov.hh	\
		   statest/west_cov.hh		\
		   statest/sample_cov.hh	\
		   statest/estimator.hh		\
		   statest/derivatives.hh	\
		   statest/sub_samples.hh	\
		   statest/bootstrap.hh		\
		   statest/jackknife.hh
JSON		:= json/json.hh
HEADERS		:= $(STAT)
DHEADERS	:= $(patsubst %.hh, %.hh.d, $(HEADERS) $(JSON))
TESTS		:= tests/test_estimator.cc	\
		   tests/test_stat.hh		\
		   tests/test_stat.cc
EXAS		:= examples/welford_var.cc	\
		   examples/welford_cov.cc	\
		   examples/west_var.cc		\
		   examples/west_cov.cc		\
		   examples/sample_var.cc	\
		   examples/sample_cov.cc	\
		   examples/derivatives.cc	\
		   examples/jackknife.cc	\
		   examples/bootstrap.cc	\
		   examples/estimator.cc	\
		   examples/catastrophic.cc	\
		   example/lsat_gpa.cc		\
		   example/nerve.cc		\
		   example/fda.cc		\
		   examples/util.hh
MDS		:= README.md			\
		   examples/README.md		\
		   tests/README.md
DOCS		:= doc/style.css		\
		   doc/layout.xml		
EXTRA		:= Makefile 			\
		   doc/Doxyfile.in 		\
		   COPYING

# --- Derived settings -----------------------------------------------
TMDS		:= $(addsuffix .pmd, $(basename $(MDS)))
TEXEC		:= $(notdir $(basename $(filter %.cc, $(TESTS))))
EEXEC		:= $(notdir $(basename $(filter %.cc, $(EXAS))))

# === Pattern rules ==================================================
# With -pg output to gmon.out or ${GMON_OUT_PREFIX}.[PID] if set
%.o:tests/%.cc
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%.o:examples/%.cc
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%.hh.gch:%.hh
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

%.hh.d:%.hh
	$(call SHOW, CPP, $@)
	$(MUTE)$(CPP) $(CPPFLAGS) -MM -MT $<.gch -o $@ $<

%:%.o
	$(call SHOW, LD, $@)
	$(MUTE)$(LD) $(LDFLAGS) -o $@ $^

%.prof:%
	$(call SHOW, PROF, $@)
	$(MUTE)LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libprofiler.so	\
	CPUPROFILE=$@						\
		./$< -a $* $(FLOWFLAGS)

%_prof.dot:%.prof
	$(call SHOW, PROF-DOT, $@)
	$(MUTE)google-pprof -dot main $< > $@

# === General targets ================================================
all:	$(TEXEC) $(EEXEC)

dep:	$(HEADERS)
	$(MAKE) $(DHEADERS)
	$(call SHOW DEPEND,$@)
	$(MUTE)./scripts/depend.py $(DHEADERS) > $@
	$(MUTE)rm -f $(DHEADERS)

install: 
ifeq ($(PREFIX),)
	$(error PREFIX is not defined)
endif
	$(foreach f, $(HEADERS), \
	  mkdir -p $(PREFIX)/$(dir $(f)); cp $(f) $(PREFIX)/$(f);)

doc:	html/index.html

clean:
	$(call SHOW,"CLEANING", "files")
	$(MUTE)find . -name "*~" | xargs rm -f

	$(MUTE)rm -f core.* TAGS *.o *.png *.vlg *.dat *.json
	$(MUTE)rm -f a.out gmon.out *.prof 

	$(MUTE)rm -f doc/*.log doc/*.aux doc/*.out doc/*.synctex* doc/*.toc

	$(MUTE)rm -rf dep html doc/Doxyfile

	$(MUTE)rm -f $(TEXEC) $(TMDS) $(EEXEC) doc/Doxyfile
	$(MUTE)rm -rf html $(NAME)-$(VERSION)

	$(MUTE)rm -f $(HEADERS:%.hh=%.hh.gch)

distclean: clean 
	rm -rf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION).zip

# === Documentation targets ==========================================
# We have to parse the markdown to turn Gitlab MD into Doxygen MD (sigh!)
README.pmd:	README.md
	$(call SHOW,MD,$@)
	$(MUTE)sed 							\
	    -e 's/\$$`/@f$$/g'						\
	    -e 's/`\$$/@f$$/g'						\
	    -e 's,(COPYING),(@ref COPYING),'				\
	    -e 's,\[\([^]]*\)\](\([^)]*\)/),@ref \1/README.pmd "\2",'	\
	    -e 's,-master.tar.gz,-$(VERSION).tar.gz,'			\
	    -e 's,^# \(.*\),@mainpage\n[TOC]\n\n# \1 {#$*_top},'  	\
	    -e 's,^\(##*\) \([^[:space:]]*\)\(.*\),\1 \2\3 {#$*_\2},' 	\
	    < $< > $@

%.pmd:	%.md
	$(call SHOW,MD,$@)
	$(MUTE)sed 							\
	    -e 's/\$$`/@f$$/g'						\
	    -e 's/`\$$/@f$$/g'						\
	    -e 's,\[\([^]]*\)\](\([^)]*\)\.\(cc\|hh\)),@ref \2.\3 "\1",'		\
	    -e 's,^\(#\) \(.*\),echo -n "\1 \2 {#";echo "$*" | tr "[[:space:]/()]" "_"; echo "}\n\n[TOC]",e'  			\
	    < $< > $@

doc/Doxyfile:	doc/Doxyfile.in 
	$(call SHOW,SED,$@)
	$(MUTE)sed -e 's/@PACKAGE@/${NAME}/' \
		   -e 's/@VERSION@/${VERSION}/' < $< > $@

html/index.html:doc/Doxyfile $(HEADERS) $(DOCS) $(TMDS)
	$(call SHOW,DOXYGEN,$@)
	$(MUTE)doxygen doc/Doxyfile
	$(MUTE)rm -f $(TMDS)

# === Distribution targets ===========================================
distdir: $(HEADERS) $(TEXECS) $(DOCS) $(MDS)
	$(call SHOW,DIST,$@)
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)$(foreach f, $^, \
	  mkdir -p $(NAME)-$(VERSION)/$(dir $(f)); \
	  cp $(f) $(NAME)-$(VERSION)/$(f);)

dist:	distdir
	$(call SHOW,TAR,$@)
	$(MUTE)tar -czf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distcheck:dist
	$(call SHOW,DIST,$@)
	$(MUTE)tar -xzf $(NAME)-$(VERSION).tar.gz
	$(MUTE)(cd $(NAME)-$(VERSION) && \
		$(MAKE) $(MAKEFLAGS) test doc dist)
	$(MUTE)rm -rf $(NAME)-$(VERSION)


# Dependencies =======================================================
# Tests --------------------------------------------------------------
tests:			$(TEXEC)

test_stat.o:		tests/test_stat.cc		\
			tests/test_stat.hh.gch		\
			statest/west_var.hh.gch		\
			statest/west_cov.hh.gch		\
			statest/welford_var.hh.gch	\
			statest/welford_cov.hh.gch	
test_stat.o:		CXXFLAGS+=-Wno-unused-function

test_estimator.o:	tests/test_estimator.cc		\
		        statest/bootstrap.hh.gch	\
		        statest/jackknife.hh.gch	\
		        statest/derivatives.hh.gch

welford_var.o:		examples/welford_var.cc		\
		        statest/welford_var.hh.gch

welford_cov.o:		examples/welford_cov.cc		\
		        statest/welford_cov.hh.gch

west_var.o:		examples/west_var.cc		\
		        statest/west_var.hh.gch

west_cov.o:		examples/west_cov.cc		\
		        statest/west_cov.hh.gch

sample_var.o:		examples/sample_var.cc		\
			statest/sample_var.hh.gch

sample_cov.o:		examples/sample_var.cc		\
			statest/sample_cov.hh.gch


derivatives.o:		examples/derivatives.cc		\
		        statest/derivatives.hh.gch

jackknife.o:		examples/jackknife.cc		\
		        statest/jackknife.hh.gch

bootstrap.o:		examples/bootstrap.cc		\
		        statest/bootstrap.hh.gch

estimator.o:		examples/estimator.cc		\
		        statest/derivatives.hh.gch	\
		        statest/jackknife.hh.gch	\
		        statest/bootstrap.hh.gch

lsat_gpa.o:		examples/lsat_gpa.cc		\
			statest/sample_cov.hh.gch	

nerve.o:		examples/nerve.cc		\
			statest/sample_var.hh.gch	

fda.o:			examples/fda.cc			\
			statest/sample_var.hh.gch	

# Dependencies automaticallly generated by target "dep" --------------
json/json.hh.gch:                                                       \
	json/json.hh                                                    

statest/basic_cov.hh.gch:                                               \
	statest/basic_cov.hh                                            \
	statest/basic_stat.hh.gch                                       

statest/basic_stat.hh.gch:                                              \
	statest/basic_stat.hh                                           \
	statest/trait.hh.gch                                            \
	json/json.hh.gch                                                

statest/basic_var.hh.gch:                                               \
	statest/basic_var.hh                                            \
	statest/basic_stat.hh.gch                                       

statest/bootstrap.hh.gch:                                               \
	statest/bootstrap.hh                                            \
	statest/sub_samples.hh.gch                                      

statest/derivatives.hh.gch:                                             \
	statest/derivatives.hh                                          \
	statest/estimator.hh.gch                                        \
	statest/west_cov.hh.gch                                         

statest/estimator.hh.gch:                                               \
	statest/estimator.hh                                            \
	statest/basic_stat.hh.gch                                       

statest/jackknife.hh.gch:                                               \
	statest/jackknife.hh                                            \
	statest/sub_samples.hh.gch                                      \
	statest/welford_var.hh.gch                                      

statest/sub_samples.hh.gch:                                             \
	statest/sub_samples.hh                                          \
	statest/estimator.hh.gch                                        \
	statest/west_var.hh.gch                                         

statest/trait.hh.gch:                                                   \
	statest/trait.hh                                                

statest/welford_cov.hh.gch:                                             \
	statest/welford_cov.hh                                          \
	statest/basic_cov.hh.gch                                        

statest/welford_var.hh.gch:                                             \
	statest/welford_var.hh                                          \
	statest/basic_var.hh.gch                                        

statest/west.hh.gch:                                                    \
	statest/west.hh                                                 \
	statest/basic_stat.hh.gch                                       

statest/west_cov.hh.gch:                                                \
	statest/west_cov.hh                                             \
	statest/basic_cov.hh.gch                                        \
	statest/west.hh.gch                                             \
	statest/welford_var.hh.gch                                      

statest/west_var.hh.gch:                                                \
	statest/west_var.hh                                             \
	statest/basic_var.hh.gch                                        \
	statest/west.hh.gch                                             

statest/sample.hh.gch:                                                  \
	statest/sample.hh                                               \
	statest/welford_var.hh.gch                                       

statest/sample_cov.hh.gch:                                              \
	statest/sample_cov.hh                                           \
	statest/basic_cov.hh.gch                                       	\
	statest/sample.hh.gch                                           

statest/sample_var.hh.gch:                                              \
	statest/sample_var.hh                                           \
	statest/basic_var.hh.gch                                        \
	statest/sample.hh.gch                                             


#
# EOF
#

