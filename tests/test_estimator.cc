#include <random>
#include <fstream>
#include <statest/basic_stat.hh>
#include <statest/estimator.hh>
#include <statest/jackknife.hh>
#include <statest/bootstrap.hh>
#include <statest/derivatives.hh>
#include <options/options.hh>
#include "complex_normal.hh"

//--------------------------------------------------------------------
/**
 * Test
 */
template <typename T> 
struct test : public T
{
  using base=T;
  using value_type=typename base::value_type;
  using values_type=typename base::values_type;
  
  test() : base(3) {}

  value_type value(const values_type& means) const
  {
    auto a = means[0];
    auto b = means[1];
    auto c = means[2];

    return a+b*b+c*c*c;
  }
  values_type gradient(const values_type& means) const
  {
    // auto a = means[0];
    auto b = means[1];
    auto c = means[2];
    return { 1, 2.*b, 3.*c*c };
  }
};

//--------------------------------------------------------------------
/**
 * Random number generator
 */
template <typename T>
struct generator
{
  using value_type=typename statest::basic_stat<T>::value_type;
  using values_type=typename statest::basic_stat<T>::values_type;
  using weights_type=typename statest::basic_stat<T>::weights_type;

  generator(unsigned int seed)
    : _eng(),
      _xdist(0.,1.),
      _wdist(.75,1.)
  {
    _eng.seed(seed);
  }
  void operator()(values_type& x, weights_type& w)
  {
    x = 0;
    x[0] = _xdist(_eng) + 1.;
    x[1] = _xdist(_eng) / 10.;
    x[2] = _xdist(_eng) / 10. + x[0];
    for (auto& ww : w) ww = _wdist(_eng);
  }
  void run(const std::string& fname, size_t nev)
  {
    std::ofstream out(fname);
    values_type   x(3);
    weights_type  w(3);
    
    for (size_t i = 0; i < nev; i++) {
      this->operator()(x,w);
      
      for (auto& xx : x) out << xx << "\t";
      for (auto& ww : w) out << ww << "\t";
      out << "\n";
    }
    out.close();
  }	   
  std::default_random_engine            _eng;
  std::normal_distribution<value_type>  _xdist;
  std::uniform_real_distribution<>      _wdist;
};

//--------------------------------------------------------------------
/**
 * Test estimator I/O
 */
template <typename T>
void test_estimator_io(const test<T>& t)
{
  json::JSON j1 = t.to_json();
  std::stringstream s;
  s << j1;
  
  json::JSON j2;
  s >> j2;

  test<T> t2;
  t2.from_json(j2);
}

//--------------------------------------------------------------------
/**
 * Test estimator
 */
template <typename T>
typename T::result_type
test_estimator(size_t nev=1000, unsigned int seed=123456)
{
  using value_type=typename test<T>::value_type;
  using values_type=typename test<T>::values_type;
  using weights_type=typename test<T>::weights_type;
  
  test<T>                test;
  generator<value_type>  gen(seed);
  values_type            x(test.size()); 
  weights_type           w(test.size()); 

  for (size_t i = 0; i < nev; i++) {
    gen(x,w);
    test.fill(x,w);
  }

  test_estimator_io(test);
  
  return test.eval();
}

//--------------------------------------------------------------------
/**
 * Test estimator
 */
template <typename T>
typename T::result_type
test_estimator(const std::string& fname, unsigned int seed=123456)
{
  using values_type=typename test<T>::values_type;
  using weights_type=typename test<T>::weights_type;

  std::default_random_engine eng;
  eng.seed(seed);
  
  test<T>        test;
  values_type    x(test.size()); 
  weights_type   w(test.size()); 

  std::ifstream in(fname);
  while (!in.eof()) {
    for (auto& xx : x) in >> xx;
    for (auto& ww : w) in >> ww;
    test.fill(x,w);
  }

  test_estimator_io(test);
  
  return test.eval();
}


//--------------------------------------------------------------------
/**
 * Print results 
 */
template <typename T>
void print_res(const std::string& what,
	       const std::pair<T,T>& r)
{
  std::cout << std::setprecision(4)
	    << std::setw(12) << what << ": "
	    << std::setw(8)  << r.first << " +/- "
	    << std::setw(8)  << r.second << std::endl;
}

//--------------------------------------------------------------------
template <typename T>
void run(const std::string& in,
	 const std::string& out,
	 unsigned int       nev,
	 unsigned int       seed)
{
  if (!out.empty()) {
    generator<T> gen(seed);
    gen.run(out,nev);
  }
  if (!in.empty()) {
    print_res("jackknife",  test_estimator<statest::jackknife<T>>  (in,seed));
    print_res("bootstrap",  test_estimator<statest::bootstrap<T>>  (in,seed));
    print_res("derivatives",test_estimator<statest::derivatives<T>>(in,seed));
    return;
  }
  print_res("jackknife",  test_estimator<statest::jackknife<T>>  (nev,seed));
  print_res("bootstrap",  test_estimator<statest::bootstrap<T>>  (nev,seed));
  print_res("derivatives",test_estimator<statest::derivatives<T>>(nev,seed));
}

int
main(int argc, char** argv)
{
  auto& cl = options::command_line::instance(argv[0]);
  options::string_option  in ('i',"input",  "FILE",   "Input file");
  options::string_option  out('o',"output", "FILE",   "Output file");
  options::bool_option    cpl('c',"complex",          "Use complex numbers");
  options::int_option     nev('n',"events", "NUMBER","Number of events",  1000);
  options::int_option     sdd('s',"seed",   "NUMBER","Random number seed",123456);
  options::bool_option    hlp('h',"help",            "Show usage");

  if (!cl.parse(argc, argv)) return 1;

  if (hlp.value()) {
    cl.usage(std::cout);
    return 0;
  }
  
  if (cpl)
    run<std::complex<double>>(in,out,nev,sdd);
  else
    run<double>              (in,out,nev,sdd);
  
  return 0;
}
  
  

    
