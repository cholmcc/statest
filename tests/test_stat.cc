#include <options/options.hh>
#include <statest/welford_var.hh>
#include <statest/welford_cov.hh>
#include <statest/west_var.hh>
#include <statest/west_cov.hh>
#include "test_stat.hh"

//====================================================================
/** 
 * Generate a test of statistics 
 *
 * @tparam weighted If the statistics is weighted 
 * @tparam T        value type 
 * @tparam S        Statistics type 
 *
 * @param nvar size of samples 
 * @param nev  Number of events 
 * @param seed Random seed 
 *
 * @return Tester of statistics 
 */
template <typename T, template <typename> class S>
bool basic(size_t nvar, bool same, bool unt, size_t nev, unsigned int seed)
{
  auto t = basic_test<T,S>(nvar,same, unt);
  return t(nev, seed);
}

//--------------------------------------------------------------------
/** 
 * Generate a test of statistics 
 *
 * @tparam weighted If the statistics is weighted 
 * @tparam T        value type 
 * @tparam S        Statistics type 
 *
 * @param nvar size of samples
 * @param nsub Number of sub-samples
 * @param nev  Number of events
 * @param seed Random number seed 
 *
 * @return Tester of statistics 
 */
template <typename T, template <typename> class S>
bool merge(size_t nvar, bool same, bool unt, size_t nsub, size_t nev, unsigned int seed)
{
  auto t = merge_test<T,S>(nvar,same,unt);
  return t(nev, nsub, seed);
}

//====================================================================
template <typename T, template <typename> class S>
bool calc(size_t nvar, bool same, bool unt,size_t nsub, size_t nev, unsigned seed)
{
  if (nsub>0) return merge<T,S>(nvar,same,unt,nsub,nev,seed);
  else        return basic<T,S>(nvar,same,unt,     nev,seed);
}

//--------------------------------------------------------------------
template<typename T>
bool weights(bool     whts,
	     bool     unt,
	     bool     cov,
	     size_t   nvar,
	     bool     same,
	     size_t   nsub,
	     size_t   nev,
	     unsigned seed)
{
  if (whts) {
    if (cov) return calc<T,statest::west_cov>(nvar,same,unt,nsub,nev,seed);
    else     return calc<T,statest::west_var>(nvar,same,unt,nsub,nev,seed);
  }
  else {
    if (cov) return calc<T,statest::welford_cov>(nvar,same,unt,nsub,nev,seed);
    else     return calc<T,statest::welford_var>(nvar,same,unt,nsub,nev,seed);
  }
}

//--------------------------------------------------------------------
bool type(bool     cmplx,
	  bool     wht,
	  bool     unt,
	  bool     cov,
	  size_t   nvar,
	  bool     same,
	  size_t   nsub,
	  size_t   nev,
	  unsigned seed)
{
  if (cmplx)
    return weights<std::complex<double>>(wht,unt,cov,nvar,same,nsub,nev,seed);
  else
    return weights<double>              (wht,unt,cov,nvar,same,nsub,nev,seed);
}

//====================================================================
int main(int argc, char** argv)
{
  auto& cl = options::command_line::instance(argv[0]);
  options::int_option   nev('n',"events",    "EVENTS","# of events", 1000);
  options::int_option   nvr('v',"variables", "VAR",   "# of variables",3);
  options::int_option   nsb('p',"partitions","PART",  "# of partitions",0);
  options::int_option   sed('s',"seed",      "NUMBER","Random seed",123456);
  options::bool_option  cov('c',"covariance","Compute covariance",false);
  options::bool_option  wht('w',"weights",   "Use weights",       false);
  options::bool_option  unt('u',"unit",      "Use unit weights",  false);
  options::bool_option  sam('k',"same",      "Same weight",       false);
  options::bool_option  cmp('z',"complex",   "Complex values",    false);
  options::bool_option  hlp('h',"help",      "Show help",         false);
  
  if (!cl.parse(argc, argv)) 
    return 0;

  if (hlp) {
    cl.usage(std::cout);
    return 0;
  }

  return type(cmp,wht,unt,cov,nvr,sam,nsb,nev,sed) ? 0 : 1;
}
//
// EOF
//

  
  
