#ifndef COMPLEX_NORMAL_HH
#define COMPLEX_NORMAL_HH
#include <random>
#include <complex>

namespace std
{
  //==================================================================
  /**
   * Specialisation of a normal distribution for complex numbers.
   *
   * That is,
   *
   * @f[ Z\sim N(\mu,\sigma)\quad,@f$ 
   *
   * with the PDF
   *
   * @f[ P_\mathrm{N}(z) = 
   * \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac12{(z-\mu)^2}{\sigma^2}}\quad,@f]
   *
   * for @f$z,\mu,\sigma\in\mathbb{C}@f$. 
   *
   * We do this by drawing @f$ Z=X+iY@f$ where 
   *
   * @f{eqnarray}{
   *    X &\sim& N\left(\mathcal{R}(\mu),\mathcal{R}(\sigma)\right)\\
   *    Y &\sim& N\left(\mathcal{I}(\mu),\mathcal{I}(\sigma)\right)\\
   * @f}
   */
  template <>
  struct normal_distribution<std::complex<double>>
  {
    /** type of real distribution */
    using double_dist=normal_distribution<double>;
    /** type of result */
    using result_type=std::complex<double>;
    /** type of parameters */
    using param_type=std::pair<double_dist::param_type,
			       double_dist::param_type>;
    /** 
     * Constructor 
     * 
     * @param mu @f$\mu@f$ 
     * @param sigma @f$\sigma@f$
     */
    normal_distribution(double mu, double sigma)
      : _r(mu, sigma),
	_i(mu+1, sigma)
    {
    }
    /** 
     * Constructor 
     * 
     * @param mu @f$\mu@f$ 
     * @param sigma @f$\sigma@f$
     */
    normal_distribution(result_type mu, result_type sigma)
      : _r(std::real(mu), std::real(sigma)),
	_i(std::imag(mu), std::imag(sigma))
    {
    }
    /** 
     * Reset state 
     */
    void reset()
    {
      _r.reset();
      _i.reset();
    }
    /** 
     * Sample 
     *
     * @param g Random number engine 
     *
     * @return The random variable @f$ Z@f$ 
     */
    template <class Generator>
    result_type operator()(Generator& g)
    {
      auto r = _r(g);
      auto i = _i(g);
      return result_type(r,i);
    }
    /** Get the mean (@f$\in\mathbb{C}@f$) */
    result_type mean() const { return result_type(_r.mean(),_i.mean()); }
    /** Get the standard deviation (@f$\in\mathbb{C}@f$) */
    result_type stddev() const { return result_type(_r.stddev(),_i.stddev()); }
    /** Get the parameters */
    param_type param() const {  return make_pair(_r.param(), _i.param()); }
    /** 
     * Set the parameters 
     *
     * @param p Parameters 
     */
    void param(const param_type& p) { _r.param(p.first); _i.param(p.second); }
    /** Get the least possible value (@f$\in\mathbb{C}@f$) */
    result_type min() const { return result_type(_r.min(),_i.min()); }
    /** Get the largest possible value (@f$\in\mathbb{C}@f$) */
    result_type max() const { return result_type(_r.max(),_i.max()); }
    /** real part random distribution */
    double_dist _r;
    /** Imaginary part random distribution */
    double_dist _i;
  };
}

#endif
//
// EOF
//
