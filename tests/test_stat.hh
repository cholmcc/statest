#ifndef TESTSTAT_HH
#define TESTSTAT_HH
#include <iostream>
#include <iomanip>
#include <sstream>
#include <statest/welford_var.hh>
#include <statest/welford_cov.hh>
#include <statest/west_var.hh>
#include <statest/west_cov.hh>
#include <progress/progress.hh>
#include "complex_normal.hh"

using namespace statest;

//====================================================================
template <typename T>
bool compare_var(const basic_stat<T>& ref,
		 const basic_stat<T>& oth,
		 double aeps,
		 double reps)
{
  auto rm = ref.mean();
  auto om = oth.mean();
  auto rv = ref.variance();
  auto ov = oth.variance();
  bool re = true;
  std::cout << std::scientific << std::setprecision(8);

  for (size_t i = 0; i < rm.size(); i++) {
    bool cm = trait<T>::is_close(rm[i], om[i], aeps, reps);
    bool cv = trait<T>::is_close(rv[i], ov[i], aeps, reps);
    if (cm && cv) continue;
    std::cout << std::setw(3) << i << ' ';
    if (!cm) 
      std::cout << "M "
		<< std::setw(12) << rm[i] << " versus "
		<< std::setw(12) << om[i] << " -> "
		<< std::setw(12) << std::fabs(om[i]-rm[i]) / std::fabs(rm[i]) << "\t"
		<< std::setw(12) << std::fabs(om[i]-rm[i])
		<< ' ';
    if (!cv) 
      std::cout << "V "
		<< std::setw(12) << rv[i] << " versus "
		<< std::setw(12) << ov[i] << " -> "
		<< std::setw(12) << std::fabs(ov[i]-rv[i]) / std::fabs(rv[i]) << "\t"
		<< std::setw(12) << std::fabs(ov[i]-rv[i])
		<< ' ';
    std::cout << std::endl;
    re = false;
  }
  std::cout << std::defaultfloat << std::setprecision(0);
  return re;
}

//--------------------------------------------------------------------
template <typename T>
typename basic_cov<T>::values_type
transpose(const basic_cov<T>& c)
{
  auto m = c.covariance();
  for (size_t i = 0; i < c.size(); i++) 
    for (size_t j = i+1; j < c.size(); j++)
      std::swap(m[i*c.size()+j],m[j*c.size()+i]);
  return m;
}

//--------------------------------------------------------------------
template <typename T>
bool compare_cov(const basic_cov<T>& ref,
		 const basic_cov<T>& oth,
		 double              aeps,
		 double              reps)
{
  // std::cout << std::scientitic << std::setprecision(8);
  // std::cout << std::defaultfloat << std::setprecision(0);
  auto rm = ref.covariance();
  if (trait<T>::is_asym(rm, aeps, reps)) {
    std::cout << "ref asymmetric" << std::endl;
    auto rt  =  transpose(ref);
    pm("rt ", rt);
    auto prd =  rm - rt;
    pm("prd ", prd);
    auto rd  =  std::abs(prd);
    pm("rd ", rd);
    auto rs  =  rd / std::abs(rm);
    pm("ref",rs,15,8);
  }
  // else
  //   std::cout << "ref symmetric" << std::endl;
  auto om = oth.covariance();
  if (trait<T>::is_asym(om, aeps, reps)) {
    auto ot =  transpose(oth);
    auto od =  std::abs(om-ot);
    auto os =  od / std::abs(om);
    pm("oth",os,15,8);
  }
  // else
  //  std::cout << "oth symmetric" << std::endl;


  return true;
}

//____________________________________________________________________
template <typename T>
bool compare(const basic_var<T>& ref,
	     const basic_var<T>& oth,
	     double              aeps=1e-10,
	     double              reps=1e-10)
{
  return compare_var(ref, oth, aeps, reps);
}
//____________________________________________________________________
template <typename T>
bool compare(const basic_cov<T>& ref,
	     const basic_cov<T>& oth,
	     double              aeps=1e-10,
	     double              reps=1e-10)
{
  bool v = compare_var(ref,oth,aeps,reps);
  bool c = compare_cov(ref,oth,aeps,reps);
  return v && c;
}

//====================================================================
/** 
 * Print out a statistics object 
 *
 * @param s Object to print 
 */
template <typename T>
void print_stat(basic_stat<T>& s)
{
  std::cout << std::setw(12) << std::setprecision(8) << std::fixed;
  s.print(std::cout, 0x3);
}
//--------------------------------------------------------------------
/** 
 * Print out a statistics object - complex specialisation
 *
 * @param s Object to print 
 */
template <>
void print_stat<std::complex<double>>(basic_stat<std::complex<double>>& s)
{
  std::cout << std::setw(17) << std::setprecision(4) << std::fixed;
  s.print(std::cout, 0x3);
}

//====================================================================
template <typename T,
	  template <typename> class S>
struct base_test
{
  /** Type of statistics */
  using stat_type=S<T>;
  /** Type of value vector  @f$ X@f$ */
  using values_type=typename stat_type::values_type;
  /** Type of value vector  @f$ X@f$ */
  using weights_type=typename stat_type::weights_type;
  /** Random number generator */
  std::default_random_engine        _eng;
  /** X distribution */
  std::normal_distribution<T>       _xdist;
  /** X distribution */
  /** Weights */
  std::uniform_real_distribution<>  _wdist;
  /** Same weight */
  bool _same;
  /** The statistics */
  stat_type _s;
  /** 
   * Constructor 
   * 
   * @param n The size of statistics to use 
   * @param same Same weight for all 
   * @param unit Unit weights
   */
  base_test(size_t n,bool same, bool unit=false)
    : _eng(),
      _xdist(0,1),
      _wdist(unit ? 1 : .75,1),
      _same(same),
      _s(n)
  {}
  /** Move */
  base_test(base_test&& o)
    : _eng(std::move(o._eng)),
      _xdist(std::move(o._xdist)),
      _wdist(std::move(o._wdist)),
      _same(o._same),
      _s(std::move(o._s))
  {}
  /** 
   * Run the test 
   *
   * @param nev  Number of events @f$ M@f$ 
   * @param seed Random number seed 
   */
  void loop(size_t nev=1000, unsigned int seed=123456)
  {
    _eng.seed(seed);
    
    size_t           frq = std::max(nev/100,1ul);
    progress::meter  pm(frq,std::cout);

    values_type  x(_s.size()); 
    weights_type w(_s.size()); 
    
    for (size_t i = 0; i < nev; i++) {
      auto cw = _wdist(_eng);
      for (auto& xx : x) xx = _xdist(_eng);
      for (auto& ww : w) ww = _same ? cw : _wdist(_eng);

      fill(x,w);
      
      pm.print(i+1,nev);
    }
    pm.print(nev,nev);
  }
  /** Fill statistics */
  virtual void fill(const values_type& x, const weights_type& w)
  {
    _s.fill(x,w);
  }
};

//====================================================================
template <typename T,
	  template <typename> class S>
struct basic_test : public base_test<T,S>
{
  /** Base type */
  using base=base_test<T,S>;
  /** Stat type */
  using stat_type=typename base::stat_type;
  /** Constructor */
  basic_test(size_t n, bool same, bool unt) : base(n,same, unt) {}
  /** Move */
  basic_test(basic_test&& o) : base(std::move(o)) {}
  /** Run the test */
  bool operator()(size_t nev=1000, unsigned int seed=123456)
  {
    this->loop(nev,seed);

    json::JSON ojson = this->_s.to_json();
    std::stringstream s;
    s << std::scientific << std::setprecision(12)
      << ojson << std::endl;
    
    stat_type o(0);
    json::JSON ijson;
    s >> ijson;
    o.from_json(ijson);

    return compare(o, this->_s);
  }
};
//====================================================================
template <typename T,
	  template <typename> class S>
struct merge_test : public base_test<T,S>
{
  /** Base type */
  using base=base_test<T,S>;
  /** Stat type */
  using stat_type=typename base::stat_type;
  /** Vector of stats */
  using stats_type=std::vector<stat_type>;
  /** Values type */
  using values_type=typename base::values_type;
  /** Weights type */
  using weights_type=typename base::weights_type;
  using base::_s;
  /** Stats */
  stats_type _sub;
  /** Sub distributions */
  std::poisson_distribution<> _sdist;
  
  /** Constructor */
  merge_test(size_t n, bool same, bool unt) : base(n,same,unt), _sub(), _sdist(2)  {}
  /** Move */
  merge_test(merge_test&& o) :
    base(std::move(o)),
    _sub(std::move(o._sub)),
    _sdist(std::move(o._sdist))
  {}
  /** Run the test */
  bool operator()(size_t nev=1000, size_t nsub=1, unsigned int seed=123456)
  {
    for (size_t i = 0; i < nsub; i++) _sub.emplace_back(_s.size());
    
    this->loop(nev,seed);

    stat_type t(_s.size());
    for (auto& si : _sub) t.merge(si);

    std::cout << "Merged over " << nsub << " partitions:" << std::endl;
    for (auto& si : _sub)
      std::cout << si.count() << std::endl;

    bool ok = compare(t, _s);
    std::cout << (ok ? "OK" : "Bad") << std::endl;

    print_stat(_s);
    print_stat(t);
    
    return ok;
  }
  /** fill in */
  void fill(const values_type& x, const weights_type& w) override
  {
    base::fill(x,w);
    size_t j = _sdist(this->_eng) % _sub.size();
    _sub[j].fill(x,w);
  }
};

#endif

  
  
  
