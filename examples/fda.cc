/**
 * @file   examples/lsat_gpa.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example from 'All About Statistics'
 */
#include <statest/sample_cov.hh>
#include <statest/welford_var.hh>
#include "util.hh"
#include <random>

using var_type  = std::valarray<double>;
using data_type = std::valarray<std::valarray<double>>;

/** 
 * The estiamtor (ratio of mean values of old-placebo and new-old)
 */
double est(const data_type& data)
{
  statest::sample_cov<> s(2,1);
  s = data;

  auto rho = s.correlation();
  return s.mean()[0]/s.mean()[1];
}

int main(int argc, char** argv)
{
  unsigned                   t = (argc > 1 ? std::stoul(argv[1]) : 0);
  size_t                     b = (argc > 1 ? std::stoul(argv[1]) : 1000);
  std::random_device         rnd;
  std::default_random_engine gen(t == 0 ? rnd() : t);

  var_type placebo = { 9243, 9671,11792,13357, 9055, 6290,12412,18806 };
  var_type old     = {17649,12013,19979,21816,13850, 9806,17208,29044 };
  var_type nw      = {16449,14614,17274,23798,12560,10157,16570,26325 };
  var_type z       = old - placebo;
  var_type y       = nw - old;
  
  data_type data(placebo.size());
  for (size_t i = 0; i < data.size(); i++)
    data[i] = {y[i], z[i]};

  auto theta  = est(data);
  auto seboot = statest::sample<>::bootstrap::estimate(data,b,est,gen);
  auto sejack = statest::sample<>::jackknife::estimate(data,est);
  
  std::cout << theta << " +/- "
	    << seboot << "(bootstrap) "
	    << sejack << "(jackknife)" << std::endl;
  
  return 0;
}
  

  

  
