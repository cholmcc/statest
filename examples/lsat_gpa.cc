/**
 * @file   examples/lsat_gpa.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example from 'All About Statistics'
 */
#include <statest/sample_cov.hh>
#include <statest/welford_var.hh>
#include "util.hh"
#include <random>

using data_type = std::valarray<std::valarray<double>>;

/** 
 * The estiamtor (correlation coefficent between LSAT and GPA scores) 
 */
double est(const data_type& data)
{
  statest::sample_cov<> s(2,1);
  s = data;

  auto rho = s.correlation();
  return rho[2 * 0 + 1];
}

int main(int argc, char** argv)
{
  unsigned                   t = (argc > 1 ? std::stoul(argv[1]) : 0);
  size_t                     b = (argc > 1 ? std::stoul(argv[1]) : 1000);
  std::random_device         rnd;
  std::default_random_engine gen(t == 0 ? rnd() : t);
  
  data_type data = {
      { 576., 3.39 },
      { 635., 3.30 },
      { 558., 2.81 },
      { 578., 3.03 },
      { 666., 3.44 },
      { 580., 3.07 },
      { 555., 3.00 },
      { 661., 3.43 },
      { 651., 3.36 },
      { 605., 3.13 },
      { 653., 3.12 },
      { 575., 2.74 },
      { 545., 2.76 },
      { 572., 2.88 },
      { 594., 2.96 }};

  auto theta  = est(data);
  auto seboot = statest::sample<>::bootstrap::estimate(data,b,est,gen);
  auto sejack = statest::sample<>::jackknife::estimate(data,est);
  
  std::cout << theta << " +/- "
	    << seboot << "(bootstrap) "
	    << sejack << "(jackknife)" << std::endl;
  
  return 0;
}
  

  

  
