/**
 * @file   examples/welford_var.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example unweighted variance 
 */
#include <statest/welford_var.hh>
#include <random>

int main(int argc, char** argv)
{
  unsigned                   t = argc > 1 ? std::stoul(argv[1]) : 0;
  size_t                     n = argc > 2 ? std::stoul(argv[2]) : 100;
  size_t                     m = argc > 3 ? std::stoul(argv[3]) : 3;
  std::random_device         rnd;
  std::default_random_engine gen(t==0 ? rnd() : t);
  std::normal_distribution<> xdist(0,1);
  std::valarray<double>      x(m);
  statest::welford_var<>     s(x.size());
      
  for (size_t i = 0; i < n; i++) {
    for (auto& xx : x) xx = xdist(gen);
    s.fill(x);
  }
  s.print();
  return 0;
}
