/**
 * @file   examples/estimator.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example of a custom estimator
 */
#include <statest/derivatives.hh>
#include <statest/jackknife.hh>
#include <statest/bootstrap.hh>
#include <cmath>
#include <random>

template <typename U>
struct est : public U
{
  using base=U;
  using value_type=typename base::value_type;
  using values_type=typename base::values_type;
  using weights_type=typename base::weights_type;
  
  est(const std::string& name) : U(1), _name(name) {}

  value_type value(const values_type& means) const
  {
    return std::exp(means[0]);
  }
  std::string _name;
};

template <typename U>
std::ostream& operator<<(std::ostream& o, const est<U>& e)
{
  auto r = e.eval();
  return o << std::setw(12) << e._name << ":"
	   << std::setprecision(4)
	   << std::setw(8)  << r.first << " +/- "
	   << std::setw(8)  << r.second;
}

int main(int argc, char** argv)
{
  int seed = argc > 1 ? std::atoi(argv[1]) : 123456;
  std::default_random_engine                eng(seed);
  std::normal_distribution<double>          xdist(5,1);

  est<statest::derivatives<>>               d("derivatives");
  est<statest::bootstrap<>>                 b("bootstrap");
  est<statest::jackknife<>>                 j("jackknife");

  est<statest::derivatives<>>::values_type  x(d.size());
  est<statest::derivatives<>>::weights_type w(d.size());

  for (size_t i = 0; i < 100; i++) {
    x[0] = xdist(eng);
    w[0] = 1;

    d.fill(x,w);
    b.fill(x,w);
    j.fill(x,w);
  }

  std::cout << d << "\n"
	    << b << "\n"
	    << j << std::endl;

  return 0;
}
  

  

  
