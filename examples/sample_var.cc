/**
 * @file   examples/sample_var.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example unweighted variance 
 */
#include <statest/sample_var.hh>
#include <random>

int main(int argc, char** argv)
{
  unsigned                   t = argc > 1 ? std::stoul(argv[1]) : 0;
  size_t                     n = argc > 2 ? std::stoul(argv[2]) : 100;
  size_t                     m = argc > 3 ? std::stoul(argv[3]) : 3;
  std::random_device         rnd;
  std::default_random_engine gen(t==0 ? rnd() : t);
  std::normal_distribution<> xdist(0,1);
  std::valarray<double>      x(n*m);
  std::valarray<double>      y(n*m);
  statest::sample_var<>      s(m);

  for (auto& xx : x) xx = xdist(gen);
  for (size_t i = 0; i < m; i++)
    y[std::slice(i*n,n,1)] = x[std::slice(i,n,m)];

  s = y;
  s.print();
  
  return 0;
}
