# Examples of using Statistical tools for estimators 

## Examples of using mean and variance or covariance algorithms 

- [Unweighted variance](welford_var.cc)
- [Unweighted covariance](welford_cov.cc)
- [Weighted variance](west_var.cc)
- [Weighted covariance](west_cov.cc)

## Examples of using estimators 

- [Test estimator](estimator.cc)
- [Bootstrap method](bootstrap.cc)
- [Jackknife method](jackknife.cc)
- [Derivatives method](derivatives.cc)

## Other examples 

- [Catastrophic cancellation](catastrophic.cc)
