/**
 * @file   examples/util.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief simple statiscal utilities 
 */
#ifndef STATEST_EXAMPLES_UTIL_HH
#define STATEST_EXAMPLES_UTIL_HH
#include <valarray>
#include <iostream>
#include <cmath>

/** 
 * Calculate the mean 
 * 
 * @f[ \bar{x} = \frac{1}{N}\sum_{i=1}^N x_i\quad, @f]
 * 
 * of a sample 
 *
 * @f[\{x_1,\ldots,x_N\}@f]
 *
 * @param data Sample 
 *
 * @return @f$\bar{x}@f$ 
 *
 * @deprecated statest::sample_var 
 */
double mean(const std::valarray<double>& data)
{
  return data.sum() / data.size();
}

/** 
 * Calculate Bessel's correction for a sample of size @f$ N@f$ 
 *
 * @f[ C=\frac{N}{N-\delta}@f] 
 *
 * where typically @f$\delta=1@f$ 
 *
 * @param data Sample 
 * @param ddof Degrees of freedom off-set 
 *
 * @return @f$ C@f$ 
 *
 * @deprecated statest::sample_var 
 */
double bessels_correction(const std::valarray<double>& data, size_t ddof=1)
{
  return double(data.size()) / (data.size() - ddof);
}

/** 
 * Calculate the variance of a sample 
 *
 * @f[\{x_1,\ldots,x_N\}@f]
 *
 * by means of residuals  
 *
 * @f[ \mathrm{var}_{\mathrm{r}}(x)=C\frac{1}{N}\sum_{i=1}^N(x_i-\bar{x})^2 @f] 
 *
 * where 
 *
 * @f[C=\frac{N}{N-\delta}\quad,@f]
 *
 * is Bessel's correction.
 *
 * @param data Sample 
 * @param avg  @f$\bar{x}@f$ 
 * @param ddof Degrees-of-freedom offset @f$\delta@f$ 
 *
 * @return @f$\mathrm{var}_{\mathrm{r}}(x}@f$ 
 *
 * @deprecated statest::sample_var 
 */
double var_residuals(const std::valarray<double>& data,
		     double avg, size_t ddof=0)
{
  return bessels_correction(data, ddof) * mean(std::pow(data - avg,2));
}

/** 
 * Calculate the variance of a sample 
 *
 * @f[\{x_1,\ldots,x_N\}@f]
 *
 * by means of difference 
 *
 * @f[ \mathrm{var}_{\mathrm{d}}(x)=C\left(\bar{x^2}-\bar{x}^2\right)@f]
 *
 * where 
 *
 * @f[C=\frac{N}{N-\delta}\quad,@f]
 *
 * is Bessel's correction.
 * @param data Sample 
 * @param avg  @f$\bar{x}@f$ 
 * @param ddof Degrees-of-freedom offset @f$\delta@f$ 
 *
 * @return @f$\mathrm{var}_{\mathrm{d}}(x)@f$ 
 *
 * @deprecated statest::sample_var 
 */
double var_difference(const std::valarray<double>& data,
		      double avg, size_t ddof=0)
{
  return bessels_correction(data,ddof) * (mean(std::pow(data,2)) - avg*avg);
}

/** 
 * Calculate the covariance between two samples 
 *
 * @param x     X sample 
 * @param y     Y sample 
 * @param xavg  Average of X sample 
 * @param yavg  Average of Y sample 
 * @param ddof  Degrees-of-freedom offset @f$\delta@f$ 
 *
 * @return @f$ \mathrm{Cov}_{xy}@f$ - the covarince 
 *
 * @deprecated statest::sample_var 
 */
double covar(const std::valarray<double>& x,
	     const std::valarray<double>& y,
	     double                       xavg,
	     double                       yavg,
	     size_t                       ddof=0)
{
  return bessels_correction(x,ddof)*mean((x-xavg)*(y-yavg));
}

/** 
 * Calculate the correlation between two samples 
 *
 * @param x     X sample 
 * @param y     Y sample 
 * @param xavg  Average of X sample 
 * @param yavg  Average of Y sample 
 * @param xvar  Variance of X sample 
 * @param yvar  Variance of Y sample 
 * @param ddof  Degrees-of-freedom offset @f$\delta@f$ 
 *
 * @return @f$ \rho_{xy}@f$ - the correlation coefficient 
 *
 * @deprecated statest::sample_var 
 */
double corr(const std::valarray<double>& x,
	    const std::valarray<double>& y,
	    double                       xavg,
	    double                       yavg,
	    double                       xvar,
	    double                       yvar,
	    size_t                       ddof=0)
{
  return covar(x,y,xavg,yavg,ddof) / sqrt(xvar*yvar);
}

/**
 * Print out result of calculation 
 *
 * @param mean   Average @f$ \bar{x}@f$ 
 * @param var    Variance @f$ \mathrm{var}(x)@f$ 
 * @param varalt Alternative variance @f$ \mathrm{var}(x)@f$ 
 */
void pr(double mean, double var, double varalt=-1)
{
  std::cout << std::setprecision(12) << std::left
	    << "<x>="         << std::setw(14) << mean << " "
	    << "<(x-<x>)^2>=" << std::setw(8) << var << " ";
  if (varalt >= 0) 
    std::cout << "<x^2>-<x>^2=" << std::setw(8) << varalt;
  std::cout << std::endl;
}

#endif
