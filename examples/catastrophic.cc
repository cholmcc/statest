/**
 * @file   examples/catastrophic.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example showing catastropic cancellation
 */
#include <statest/welford_var.hh>
#include <statest/sample_var.hh>

/**
 * Print out result of calculation 
 *
 * @param mean   Average @f$ \bar{x}@f$ 
 * @param var    Variance @f$ \mathrm{var}(x)@f$ 
 * @param altvar Alternative variance @f$ \mathrm{var}(x)@f$ 
 */
void pr(double mean, double var, double varalt=-1)
{
  std::cout << std::setprecision(12) << std::left
	    << "<x>="         << std::setw(14) << mean << " "
	    << "<(x-<x>)^2>=" << std::setw(8) << var << " ";
  if (varalt >= 0) 
    std::cout << "<x^2>-<x>^2=" << std::setw(8) << varalt;
  std::cout << std::endl;
}

/** 
 * for one sample 
 *
 * @f[\{x_1,\ldots,x_N\}@f]
 *
 * calculate 
 *
 * - the mean @f$\bar{x}@f$, 
 *
 * and the variances 
 *
 * - @f$\mathrm{var}_{\mathrm{r}}(x)@f$ and 
 * - @f$\mathrm{var}_{\mathrm{d}}(x)@f$ 
 *
 * using statest::sample_var, as well as the mean and variance using
 * Welford's algorithm (statest::welford_var).
 *
 * @param data Sample 
 * @param ddof Degrees of freedom offset @f$\delta@f$ 
 *
 */
void one(const std::valarray<double>& data,size_t ddof=0)
{
  statest::welford_var<> w(1,ddof);
  statest::sample_var<>  n(1,ddof,false);
  statest::sample_var<>  a(1,ddof,true);
  for (auto v : data) w.fill({v});
  n = data;
  a = data;

  std::cout << "Normal  ";
  pr(n.mean()[0],n.variance()[0],a.variance()[0]);
  std::cout << "Welford ";
  pr(w.mean()[0], w.variance()[0]);
}

/** 
 *
 * Run test on the sample 
 *
 * @f$ X=\{4,7,13, 16\}@f$ 
 *
 * as well as 
 *
 * @f$ Y=\{x_i+100\,000\,000\|i=1,\ldots 4\}@f$ 
 */
int main()
{
  std::valarray<double> data1 = { 4 ,7 ,13 ,16 };
  one(data1);

  auto data2 = data1 + 1e8;
  one(data2);

  return 0;
}

//
// EOF
//
