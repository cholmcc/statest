/**
 * @file   examples/west_cov.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example weighted covariance 
 */
#include <statest/west_cov.hh>
#include <random>

int main(int argc, char** argv)
{
  unsigned                         t = argc > 1 ? std::stoul(argv[1]) : 0;
  size_t                           n = argc > 2 ? std::stoul(argv[2]) : 100;
  size_t                           m = argc > 3 ? std::stoul(argv[3]) : 3;
  std::random_device               rnd;
  std::default_random_engine       gen(t==0 ? rnd() : t);
  std::normal_distribution<>       xdist(0,1);
  std::uniform_real_distribution<> wdist(0,1);
  std::valarray<double>            x(m);
  std::valarray<double>            w(m);
  statest::west_cov<>              s(x.size());
      
  for (size_t i = 0; i < n; i++) {
    for (auto& xx : x) xx = xdist(gen);
    for (auto& ww : w) ww = wdist(gen);
    s.fill(x);
  }
  s.print();
  return 0;
}
