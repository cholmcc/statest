/**
 * @file   examples/derivatives.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Example of using derivatives to estimate uncertainties. 
 */
#include <statest/derivatives.hh>
#include <random>

struct test : public statest::derivatives<>
{
  using base=statest::derivatives<>;
  using values_type=typename base::values_type;
  using weights_type=typename base::weights_type;
  
  test() : base(3) {}

  value_type value(const values_type& means) const
  {
    auto a = means[0];
    auto b = means[1];
    auto c = means[2];

    return a+b*b+c*c*c;
  }
  values_type gradient(const values_type& means) const
  {
    // auto a = means[0];
    auto b = means[1];
    auto c = means[2];
    return { 1, 2.*b, 3.*c*c };
  }
};

int main()
{
  std::random_device                    rd;
  std::default_random_engine            eng(rd());
  std::normal_distribution<double>      xdist(0,1);
  std::uniform_real_distribution<>      wdist(0.75,1);

  test                                  t;
  test::values_type                     x(t.size());
  test::weights_type                    w(t.size());
  
  for (size_t i = 0; i < 1000; i++) {
    x = 0;
    x[0] = xdist(eng) + 1.;
    x[1] = xdist(eng) / 10.;
    x[2] = xdist(eng) / 10. + x[0];
    for (auto& ww : w) ww = wdist(eng);

    t.fill(x,w);
  }

  test::result_type r = t.eval();
  std::cout << std::setprecision(4)
	    << std::setw(8)  << r.first << " +/- "
	    << std::setw(8)  << r.second << std::endl;
  
    
  return 0;
}
