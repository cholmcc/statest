/**
 * @file   statest/west_cov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief West update for mean and covariance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WEST_COV_HH
#define STAT_WEST_COV_HH
#include <statest/basic_cov.hh>
#include <statest/west.hh>
#include <statest/welford_var.hh>

namespace statest
{
  /**
   * Online calculation of means and covariance with component,
   * non-frequency weights.
   *
   * For a weighted sample 
   *
   * @f[X=\left\{(x_1,w_1),\ldots,(x_N,w_N)\right\}\quad,@f] 
   *
   * the mean and covariance is given by 
   *
   @f{align*}{
    \omega  &= \begin{cases} w & \text{observation weights}\\ ww^T & \text{component weights}\end{cases}\\
    W_{1,n}        &= W_{1,n-1} + \omega\\
    W_{2,n}        &= W_{2,n-1} + \omega^2\\
    W_n            &= W_{1,n-1} + w\\
    \overline{x}_n &= \overline{x}_{n-1} + \frac{w}{W_n}(x - \overline{x}_{n-1}) \\
    c_{n}          &= \frac{\Delta_{n-1}}{\Delta_{n}}c_{n-1} + \frac{\omega}{\Delta_n}(x - \overline{x}_{n-1})(x - \overline x_n)^T\\
    \Delta_n       &= W_{1,n} - 
                      \begin{cases}
                        0 & \text{biased frequency weights}\\
                        1 & \text{frequency weights}\\
                        \frac{W_{2,n}}{W_{1,n}}
                      \end{cases}\quad.
   @f}
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   *
   * @include examples/west_cov.cc
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean |   uncer. |        0 |        1 |        2 |        3
   -----+----------+----------+----------+----------+----------+----------
      0 |     0.07 |     0.09 |     0.99 |     0.01 |    -0.10 |    -0.11
      1 |    -0.19 |     0.09 |     0.01 |     0.80 |    -0.11 |    -0.00
      2 |    -0.09 |     0.10 |    -0.10 |    -0.11 |     0.94 |     0.11
      3 |     0.18 |     0.11 |    -0.11 |    -0.00 |     0.11 |     1.19
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/west_cov.hh>
   *
   */
  template <typename T=double>
  struct west_cov : public basic_cov<T>, public west<T> 
  {
    /** base type  */
    using base=basic_cov<T>;
    /** utility */
    using util=west<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** Vector of reals type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** Vector of reals type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** Vector of reals type */
    using reals_type=typename base::reals_type;
    /** Unweighted variance calculator type */
    using var=welford_var<T>;
  
    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    west_cov(size_t n=3,size_t ddof=0)
      : base(n,ddof),
	_sumww(n*n),
	_sumw2(n*n),
	_sumw(n),
	_var(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    west_cov(west_cov&& o)
      : base(std::move(o)),
	_sumww(std::move(o._sumww)),
	_sumw2(std::move(o._sumw2)),
	_sumw(std::move(o._sumw)),
	_var(std::move(o._var))
    {}
    /** Deleted copy constructor */
    west_cov(const west_cov&) = delete;
    /** Deleted assignment operator */
    west_cov& operator=(const west_cov&) = delete;

    /** 
     * @{ 
     * @name Manipulations 
     */
    /** Reset state */
    virtual void reset() override
    {
      base::reset();
      std::fill(std::begin(_sumww),  std::end(_sumww),  0);
      std::fill(std::begin(_sumw2), std::end(_sumw2), 0);
      std::fill(std::begin(_sumw), std::end(_sumw), 0);
      _var.reset();
    }
    /** 
     * Fill in an observation.  This assumes all weights are 1. 
     *
     * @param x Obervations 
     */
    void fill(const values_type& x) override
    {
      const weights_type w(1,x.size());
      fill(x,w);
    }
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     * @param w weights
     */
    void fill(const values_type& x, const weights_type& w) override
    {
      // Fill unweighted observation
      _var.fill(x,w);
      weights_type ww = trait_type::outer(w,w);
      _fill(ww,ww*ww,w,x,value_type(0),1);
    }
    /** 
     * Merge other statistics object into this 
     *
     * @param o Other object to merge in 
     *
     * @return Reference to this 
     */
    west_cov& merge(const west_cov& o)
    {
      assert(o.size() == this->size());
      if (o._n == 0) return *this;
      _var.merge(o._var);
      _fill(o._sumww,o._sumw2,o._sumw,o._mean,o._cov,o._n);
      return *this;
    }
    /** @} */

    /** 
     * @{ 
     * @name Get information 
     */
    /** 
     * Get the uncertainty on the weighted mean
     *
     * @f[
     \mathrm{Var}[\overline{x_i}_w] = 
     \frac{\sum_j w_{ij}^2}{\left(\sum_j w_{ij}\right)^2}\mathrm{Var}[x_i]
     @f]
     * 
     * @return standard uncertainty on the mean 
     */
    reals_type sem() const override
    {
      // Take diagonal of correction matrix and multiply on standard
      // deviation of unweighted sample.
      weights_type c = cor()[std::slice(0,size(),size()+1)];
      return c*_var.std();
      // The below is _not_ correct 
      // return std() / std::sqrt(_sumww);
    }
    // Clarify scope
    using base::size;    
    /** @} */
    
    /** 
     * @{ 
     * @name JSON input/output 
     */
    // Clarify scope
    using base::to_json_array;
    using base::from_json_array;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON to_json() const override
    {
      json::JSON json = base::to_json();
      json["sumw"]  = to_json_array(_sumw);
      json["sumw2"] = to_json_array(_sumw2);
      json["sumww"] = to_json_array(_sumww);
      json["var"]   = _var.to_json();
      
      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    void from_json(const json::JSON& json) override
    {
      base::from_json(json);
      from_json_array(json["sumw"],_sumw);
      from_json_array(json["sumw2"],_sumw2);
      from_json_array(json["sumww"],_sumww);
      _var.from_json(json["var"]);
    }
    /** 
     * Merge in from JSON object 
     */
    void merge_json(const json::JSON& in)
    {
      west_cov<T> w(this->size());
      w.from_json(in);
      merge(w);
    }
    /** @} */
    
  protected:
    /** Clarify scope */
    using base::_n;
    /** Clarify scope */
    using base::_ddof;
    /** Clarify scope */
    using base::_mean;
    /** Clarify scope */
    using base::_cov;
    /** Clarify scope */
    using util::denom;
    /** Clarify scope */
    using util::scor;

    /** 
     * Actual fill method - works for both regular files and merges. 
     *
     * Here, we take @c dy _before_ the mean update.  If we took the
     * @c dy _after_ the mean update, we would end up with an
     * asymmetric covariance matrix.  This is because the weight
     * vector sumw is not the same throughout.  Thus, if we calculate
     * the outer product
     *
     * @f[ \delta \delta'^T = (x-\bar{x})(x-\bar{x}')^T\quad,@f]
     *
     * where @f$\bar{x}'@f$ is the mean after the update, then we
     * would have an asymmetric matrix which we will update by
     *
     * This means that we take @f$\delta'=\delta@f$, and the update of
     * partition @f$A@f$ by partition @f$B@f$ with (small letters
     * vectors, capital matrixes) for @f$ P=A,B@f$ 
     *
     * @f{eqnarray}{
     w_{P} &=& \sum_i w_{P,i}\\
     U_{P} &=& \sum_i w_{P,i} w_{P,i}^T\\
     V_{P} &=& \sum_i (w_{P,i} w_{P,i}^T)^2\\
     W_{P} &=& U_{P} - \delta_\nu\frac{V_{P}}{U_P}\\
     \overline{x}_P &=& \frac{\sum_i w_{P,i}x_{P,i}}{w_P}\\
     \mathrm{Cov}_P &=& \frac{\sum_i w_{P,i}[x_{P_i}-\overline{x}_P]
     \left(w_{P,i}[x_{P_i}-\overline{x}_P]\right)^T}{W_{P}}\quad,
     @f}
     *
     * (the last term in the fourth line is the bias correction, with
     * @f$\delta_\nu=0@f$ corresponding to the _biased_ estimator, and
     * @f$\delta_\nu=1@f$ to the _unbiased_ estimator)
     *
     * is done by
     *
     * @f{eqnarray}{
     w_{AB} &=& w_A + w_B\\
     U_{AB} &=& U_A + U_B\\
     V_{AB} &=& V_A + V_B\\
     W_{AB} &=& W_A + W_B - \delta_\nu\frac{V_{AB}}{U_{AB}}\\
     \delta &=& \overline{x}_A - \overline{x}_B\\
     \overline{x}_{AB} &=& \frac{\overline{x}_A + w_B \delta}{w_{AB}}\\
     \Delta &=& \frac{W_A}{W_{AB}}\delta\delta^T\\
     \mathrm{Cov}_{AB} &=& \frac{W_A\mathrm{Cov}_A+W_B\left(\mathrm{Cov}_B+\Delta\right)}{W_{AB}}\quad. 
     @f}
     *
     * Here @f$\mathrm{Cov}_P@f$ is the covariance over partition
     * @f$P@f$.
     * 
     * The above also holds for a partition @f$ B@f$ of size 1 (i.e.,
     * a single observation) with @f$\mathrm{Cov}_B=0@f$. This is the
     * primary reason why this is implemented as a template member
     * function: In case of a single observation we can pass in a
     * scalar constant instead of constructing a full null matrix.
     *
     * Clearly, @f$w_B,U_B,V_B,W_B,\overline{x}_B,@f$ and
     * @f$\mathrm{Cov}_B@f$ can be calculated using this algorithm.
     *
     * Note, for complex numbers we replace the transpose
     * @f$\cdot^T@f$ with the hermitian conjugate @f$\cdot^H@f$.
     *
     * @param sumww @f$ U_B@f$ to update by 
     * @param sumw2 @f$ V_B@f$ to update by 
     * @param sumw  @f$ w_B@f$ to update by 
     * @param mean  @f$ x@f$ or sample mean @f$\bar{x}_B@f$ to update by 
     * @param cov   0 or sample covariance @f$ \mathrm{Cov}_B@f$ to update by 
     * @param n     number of fills to update by
     */
    template <typename C>
    void _fill(const weights_type& sumww,
	       const weights_type& sumw2,
	       const weights_type& sumw,
	       const values_type&  mean,
	       const C&            cov,
	       size_t              n)
    {
      if (_n == 0) {
	_mean  = mean;
	_cov   = cov;
	_sumww = sumww;
	_sumw2 = sumw2;
	_sumw  = sumw;
	_n     = n;
	return;
      }
#if 1
      values_type  fac =  denom(_sumww, _sumw2, _ddof);
      _sumww            += sumww;
      _sumw2            += sumw2;
      _sumw             += sumw;
      values_type dx   =  mean - _mean;
      _mean             += trait_type::div(trait_type::value(sumw)*dx, _sumw);
      values_type div  =  denom(_sumww, _sumw2, _ddof);
      values_type dxy  =  trait_type::div(fac,div,value_type(1))*trait_type::outer(dx,dx);
      _cov              *= fac; 
      _cov              += trait_type::value(sumww)*(cov+dxy);
      _cov              =  trait_type::div(_cov,div,value_type(1));
      _n                += n;
#else      
      values_type  fac =  denom(_sumww, _sumw2, _ddof);
      _sumww            += sumww;
      _sumw2            += sumw2;
      _sumw             += sumw;
      values_type dx   =  mean - _mean;
      values_type dy   =  mean - _mean; // Hmm, why not after mean up?
      _mean             += trait_type::div(trait_type::value(sumw)*dx, _sumw);
      values_type dxy  =  trait_type::outer(dy,dx);
      _cov              *= fac; 
      _cov              += trait_type::value(sumww)*(cov+dxy);
      values_type div  =  denom(_sumww, _sumw2, _ddof);
      _cov              =  trait_type::div(_cov,div,value_type(1));
      _n                += n;
#endif
    }
      
    /** Get correction on standard deviation */
    weights_type cor() const
    {
      return scor(_sumww,_sumw2);
    }
    std::string id() const override { return "west_cov"; }
    /** Sum of w w^T */
    weights_type _sumww;
    /** Sum of weights squared */ 
    weights_type _sumw2;
    /** Sum of weights times means */ 
    weights_type _sumw;
    /** The unweighted mean and variance */
    var _var;
  };
  /** 
   * @example west_cov.cc
   * @ingroup statest_stat
   *
   * Example of calculating the covariance matrix from a weighted sample. 
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   * The corresponding four weights are drawn from a uniform
   * distribution
   *
   * @f[ w_i \sim \mathcal{U}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   *
   */
}
#endif
//
// EOF
//

