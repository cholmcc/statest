/**
 * @file   statest/Jackknife.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_JACKKNIFE_HH
#define CORRELATIONS_STAT_JACKKNIFE_HH
#include <statest/sub_samples.hh>
#include <statest/welford_var.hh>

namespace statest
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using jackknife estimate of uncertainties 
   *
   * Note that this method generally overestimates the uncertainties.
   *
   * See the example @ref jackknife.cc 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_uncer
   * @headerfile "" <statest/jackknife.hh>
   */
  template<typename T=double, unsigned short NSUB=10>
  struct jackknife : public sub_samples<T,NSUB>
  {
    /** Base class */
    using base=sub_samples<T>;
    /** real value type */
    using value_type=typename base::value_type;
    /** a value vector */
    using values_type=typename base::values_type;

    /** Destructor */
    virtual ~jackknife() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    jackknife(jackknife&& o)
      : base(std::move(o))
    {}
    /** 
     * Normal copy constructor (deleted)
     */
    jackknife(const jackknife&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    jackknife& operator=(const jackknife&) = delete;
  protected:
    /** Clarify scope */
    using base::_sub;
    /** Clarify scope */
    using base::_s;
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    jackknife(size_t n)
      : base(n)
    {
    }
    /** 
     * Calculate the uncertainty of the measurements using a jackknife 
     *
     * @return The uncertainty 
     */
    value_type simulate() const override
    {
      welford_var<T> s(1);
      for (size_t i = 0; i < _sub.size(); i++) {
	welford_var<T> t(1);
	for (size_t j = 0; j < _sub.size(); j++) {
	  if (i == j) continue;
	  t.fill({this->value(this->_sub[j].mean())});
	}
	s.fill({t.mean()[0]});
      }
      return std::sqrt(_sub.size()-1)*s.std()[0];
    }	    
  };
  /** 
   * @example jackknife.cc 
   *
   * We use the statest::jackknife method to estimate the uncertainty
   * on the estimator
   *
   * @f[\hat\theta = a+b^2+c^3\quad.@f]
   *
   * Each are distributed as 
   *
   * @f{align*}{
   a &\sim \mathcal{N}[0,1] + 1\\
   b &\sim \frac1{10}\mathcal{N}[0,1]\\ 
   c &\sim \frac1{10}\mathcal{N}[0,1] + a
   @f}
   *
   * Note, normal error progation yields 
   *
   * @f{align*}{
   \mathrm{var}(\hat\theta) &= 
   \sum_{x=a,b,c}\left\(\frac{\partial \hat\theta}{\partial x}\delta x\right)^2
   &= \delta a^2 + 4b^2\delta b^2+9c^4\delta c^2 
   &= 1 + 4\frac1{10^2}0\frac1{10^2}+9\cdot 1\cdot \left(1+\frac1{100}\right)
   &= 10.1809\quad,
   @f} 
   *
   * and thus, for sample of size @f$ N=1000@f$ an uncertainty of
   *
   * @f[ \delta \hat\theta^2 = \sqrt{\frac{\mathrm{var}(\hat\theta)}{N}} 
   = \sqrt{\frac{10.1809}{1000}} = 0.101@f]
   *
   * Possible output of example program
   *
   * @verbatim 
      1.954 +/-   0.1264
   @endverbatim
  */
}
#endif
//
// EOF
//

  
