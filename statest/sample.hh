/**
 * @file   statest/sample.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Utilities used by Sample updates
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_SAMPLE_HH
#define STAT_SAMPLE_HH
#include <statest/basic_stat.hh>
#include <statest/welford_var.hh>

namespace statest
{
  /** 
   * Utilities used for sample updates. 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/sample.hh>
   */
  template <typename T=double>
  struct sample
  {
    /** value type */
    using value_type=typename basic_stat<T>::value_type;
    /** value vector type */
    using values_type=typename basic_stat<T>::values_type;
    /** Type of weights */
    using weight_type=typename basic_stat<T>::weight_type;
    /** Type of weights */
    using weights_type=typename basic_stat<T>::weights_type;
    /** real type */
    using real_type=typename basic_stat<T>::real_type;
    /** trait type */
    using trait_type=typename basic_stat<T>::trait_type;

    /** Destructor */
    virtual ~sample() {}
  protected:
    /** Constructor - does nothing */
    sample() {}
    /** Move constructor - does nothing */
    sample(sample&&) {}
    /** Deleted copy constructor */
    sample(const sample&) = delete;
    /** Deleted assignment operator */
    sample& operator=(const sample&) = delete;
    /** 
     * Flatten input data to the format 
     *
     * @f[\{x_{1,1},\ldots,x_{1,N},\ldots,\{x_{M,1},\ldots,x_{M,N}\}@f]
     *
     * The values argument is a container of containers.  These can
     * have the form
     *
     * @f[\{\{x_{1,1},\ldots,x_{1,N}\},\ldots,\{x_{M,1},\ldots,x_{M,N}\}\}@f]
     *
     * or 
     *
     * @f[\{\{x_{1,1},\ldots,x_{M,1}\},\ldots,
     \{x_{1,N},\ldots,x_{M,N}\}\}\quad.@f]
     * 
     * @tparam C Outer container type 
     * @tparam D Inner container type 
     *
     * @param values container of container of values 
     * @param m      Number of components @f$ M@f$ 
     *
     * @return Flattened sample 
     */
    template <template <class ...> class C,
	      template <class ...> class D>
    values_type flatten(const C<D<value_type>>& values, size_t m) const
    {
      size_t n = std::distance(std::begin(values),std::end(values));
      bool   r = false;
      if (n == m) {
	n = std::distance(std::begin(*std::begin(values)),
			  std::end  (*std::begin(values)));
	r = true;
      }

      values_type  x(n*m);
      size_t       i = 0;
      if (r) // first case - each element is one variable
	for (auto v : values) {
	  values_type t(n);
	  std::copy(std::begin(v),
		    std::end(v),
		    std::begin(t));
		    
	  x[std::slice(n * i++, n, 1)] = t;
	  // x[std::slice(n * i++, n, 1)] = v;
	}
      else // second case - each element is an observation
	for (auto v : values) {
	  values_type t(n);
	  std::copy(std::begin(v),
		    std::end(v),
		    std::begin(t));
	  x[std::slice(i++, m, n)] = t;
	  // x[std::slice(i++, m, n)] = v;
	}
      return x;
    }
  public:
    /** Bootstrap estimate of uncertainty based on full sample access */
    struct bootstrap
    {
      /** 
       * Generate a single bootstrap sample.  That is, given the data
       * of size @f$N@f$ pick as many observations at random (with
       * replacement) from the data and return the simulated data.
       *
       * @param data The data 
       * @param g    Random number generator to use 
       *
       * @return new simulated sample
       */
      using real_type=typename sample<T>::real_type;
      template <typename Data, typename Generator>
      static std::valarray<Data> sample(const std::valarray<Data>& data,
					Generator&& g)
      {
	std::uniform_int_distribution<size_t> u(0,data.size());
	std::valarray<size_t>                 i(data.size());
	for (auto& ii : i)                    ii = u(g);

	return data[i];
      }
      /** 
       * Estimate the uncertainty using the Bootstrap prescription. 
       *
       * @param data The sample 
       * @param b    Number of simulations 
       * @param e    The estimator.  This must accept a single argument `data`. 
       * @param g    The random number generator used for generating samples
       *
       * @return The jackknife estimate of the uncertainty 
       */
      template <typename Data, typename Estimator, typename Generator>
      static real_type estimate(const std::valarray<Data>& data,
				size_t                     b,
				Estimator&&                e,
				Generator&&                g)
      {
	statest::welford_var<> w(1);
  
	for (size_t i = 0; i < b; i++)
	  w.fill({e(sample(data,g))});

	return w.std()[0];
      }
    };

    /** Jackknife estimate of uncertainty based on full sample access */
    struct jackknife
    {
      using real_type=typename sample<T>::real_type;
      /** 
       * Generate the @f$ n^{\text{th}}@f$ Jackknife sample - that is,
       * the sample were we left out the @f$ n^{\text{th}}@f$
       * observation.
       *
       * @param data The data 
       * @param n    The observation to leave out 
       *
       * @return new simulated sample with the @f$ n^{\text{th}}@f$
       * observation left out
       */ 
      template <typename Data>
      static std::valarray<Data>  sample(const std::valarray<Data>& data,
					 size_t n)
      {
	auto ret = data.cshift(n);
	return ret[std::slice(0,data.size()-1,1)];
      }
      /** 
       * Estimate the uncertainty using the Jackknife prescription. 
       *
       * @param data The sample 
       * @param e    The estimator.  This must accept a single argument `data`. 
       *
       * @return The jackknife estimate of the uncertainty 
       */
      template <typename Data, typename Estimator>
      static real_type estimate(const std::valarray<Data>& data,
				Estimator&&                e)
      {
	statest::welford_var<> w(1);
  
	for (size_t i = 0; i < data.size(); i++)
	  w.fill({e(sample(data,i))});
	
	return std::sqrt(data.size()-1)*w.std()[0];
      }
    };
  };
}
#endif
//
// EOF
//

