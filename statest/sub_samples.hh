/**
 * @file   statest/sub_samples.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_SUB_SAMPLES_HH
#define CORRELATIONS_STAT_SUB_SAMPLES_HH
#include <statest/estimator.hh>
#include <statest/west_var.hh>
#include <vector>

namespace statest
{
  // -----------------------------------------------------------------
  /** 
   * Base class for calculators using sub-samples to estimate
   * uncertainties
   *
   *
   * @tparam T The value type 
   *
   * @ingroup statest_est
   * @headerfile "" <statest/sub_samples.hh>
   */
  template<typename T=double, unsigned short NSUB=10>
  struct sub_samples : public estimator<T>
  {
    /** base class */
    using base=estimator<T>;
    /** Statistics calculator for weighted mean and covariance */
    using var_type=west_var<T>;
    /** Container of variances for each sub-sample */
    using vars_type=std::vector<var_type>;
    /** value value type */
    using value_type=typename base::value_type;
    /** A value vector */
    using values_type=typename base::values_type;
    /** weight weight type */
    using weight_type=typename base::weight_type;
    /** A weight vector */
    using weights_type=typename base::weights_type;

    /** Destructor */
    virtual ~sub_samples() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    sub_samples(sub_samples&& o)
      : _s(std::move(o._s)),
	_sub(std::move(o._sub)),
	_calc(o._calc),
	_std(o._std)
    {}
    /** 
     * Normal copy constructor (deleted)
     */
    sub_samples(const sub_samples&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    sub_samples& operator=(const sub_samples&) = delete;
    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  weights 
     */
    void fill(const values_type& x, const weights_type& w) override 
    {
      _s.fill(x,w);
      _sub[select()].fill(x,w);
      _calc = false;
    }

    /** 
     * Merge with other 
     */
    virtual void merge(const sub_samples<T,NSUB>& o)
    {
      assert(o._sub.size() == _sub.size());
      _s.merge(o._s);
      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].merge(o._sub[i]);
    }
    /** @} */

    /** 
     * @{ 
     * @name Get information 
     */
    /** 
     * Number of input parameters expected 
     */
    size_t size() const override { return _s.size(); }

    /** Dummy */
    value_type rho(size_t, size_t) const { return 0; }
      
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    const values_type& mean() const override { return _s.mean(); }
    /** @} */

    /** 
     * @{ 
     * @name Show state 
     */
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a o
     */
    std::ostream& print(std::ostream& o, unsigned int f=0x0) const override 
    {
      size_t w = o.width(0);
      o << std::setw(w);

      if (f >= base::MORE) {
	o << std::setw(0) << "N=" << _sub.size() << " " << std::setw(w);
	_s.print(o, f);
      }

      if (f >= base::DETAILS) {
	for (auto& t : _sub) {
	  o << std::setw(w);
	  t.print(o, f);
	}
      }

      o << std::setw(w);
      base::print(o);

      return o << std::endl << std::setw(w);
    }
    /** @} */

    /** 
     * @{
     * @name JSON input/output 
     */
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    json::JSON to_json() const override
    {
      json::JSON json;
      json["nsub"] = _sub.size();
      json["stat"] = _s.to_json();
      json["subs"] = json::Array();
      for (auto& s : _sub) json["subs"].append(s.to_json());

      return json;
    }
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    void from_json(const json::JSON& json) override
    {
      size_t nsub = json["nsub"].toInt();
      assert(nsub == _sub.size());

      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].from_json(json["subs"][i]);
    }
    /**
     * Merge from JSON 
     *
     * @param json 
     */
    void merge_json(const json::JSON& json) override 
    {
      _s.merge_json(json["stat"]);
      
      size_t nsub = json["nsub"].toInt();
      assert(nsub == _sub.size());
      
      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].merge_json(json["subs"][i]);
    }
    /** @} */
  protected:
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    sub_samples(size_t n)
      : base(),
	_s(n),
	_sub(),
	_calc(false),
	_std(0)
    {
      for (size_t i = 0; i < NSUB; i++) _sub.emplace_back(n);
    }
    /**
     * Get a random number between 0 and the number of samples
     * (exclusive)
     *
     * @return Random number between 0 and the number of samples 
     */
    size_t select() const
    {
      return size_t(float(rand()) / RAND_MAX * _sub.size());
    }
    /** 
     * Calculate the uncertainty of the measurements using a sub-samples 
     *
     * @return The uncertainty 
     */
    value_type uncertainty(const values_type&) const override 
    {
      if (!_calc) {
	_std = simulate();
	_calc = true;
      }
      return _std;
    }
    /** 
     * Do simulation over sub-samples 
     *
     * @return Standard deviation 
     */
    virtual value_type simulate() const = 0;
    /** Statistics  */
    var_type _s;
    /** Sub-sample statistics */
    vars_type _sub;
    /** Flag if we've done the calculation */
    mutable bool _calc;
    /** Calculated standard deviation */
    mutable value_type _std;
  };
}
#endif
//
// EOF
//

  
