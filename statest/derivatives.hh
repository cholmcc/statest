/**
 * @file   statest/Derivatives.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_DERIVATIVES_HH
#define CORRELATIONS_STAT_DERIVATIVES_HH
#include <statest/estimator.hh>
#include <statest/west_cov.hh>
#include <numeric>

namespace statest
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using derivatives for uncertainties 
   *
   * See the example @ref derivatives.cc 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_uncer
   * @headerfile "" <statest/derivatives.hh>
   */
  template <typename T=double>
  struct derivatives : public estimator<T>
  {
    using base=estimator<T>;
    /** traits */
    using trait_type=typename base::trait_type;
    /** Statistics calculator for weighted mean and covariance */
    using cov_type=west_cov<T>;
    /** value value type */
    using value_type=typename base::value_type;
    /** A value vector */
    using values_type=typename base::values_type;
    /** weight weight type */
    using weight_type=typename base::weight_type;
    /** A weight vector */
    using weights_type=typename base::weights_type;
    /** real real type */
    using real_type=typename base::real_type;
    /** A real vector */
    using reals_type=typename base::reals_type;

    /** Destructor */
    virtual ~derivatives() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    derivatives(derivatives&& o) : base(std::move(o)), _s(std::move(o._s)){}
    /** 
     * Normal copy constructor (deleted)
     */
    derivatives(const derivatives&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    derivatives& operator=(const derivatives&) = delete;
    /** 
     * Number of input arguments to average over 
     */
    size_t size() const override { return _s.size(); }
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  weights 
     */
    void fill(const values_type& x, const weights_type& w) override
    {
      this->_s.fill(x,w);
    }
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a a
     */
    std::ostream& print(std::ostream& o, unsigned int f=0x0) const override
    {
      size_t w = o.width(0);
      o << std::setw(w);

      if (f >= base::MORE) {
	_s.print(o, f);
	o << std::setw(w);
      }
      
      base::print(o, f);
      return o << std::endl << std::setw(w);
    }
    /** 
     * Get the weighted correlation between two components 
     *
     * @f[
     \rho_{ij} = 
     \frac{\mathrm{Cov}[x_i,x_j]_w}{
     \sqrt{\mathrm{Var}[x_i]_w\mathrm{Var}[x_j]_w}}
     @f]
     *
     * @param i Index 
     * @param j Index 
     *
     * @return @f$\rho_{ij}@f$ 
     */
    value_type rho(size_t i, size_t j) const
    {
      return rho()[i * _s.size() + j];
    }
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    json::JSON to_json() const override
    {
      json::JSON json = {"stat", _s.to_json() };
      return json;
    }
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    void from_json(const json::JSON& json) override
    {
      _s.from_json(json["stat"]);
    }
    /** 
     * Merge state from input stream 
     *
     * Note, we cannot use the merge function because we cannot
     * instantise this class.
     *
     * @param json JSON object
     */
    void merge_json(const json::JSON& json)
    {
      _s.merge_json(json["stat"]);
    }
    /** 
     * Merge state from other object 
     *
     * @param o Other object  
     *
     */
    void merge(const derivatives<T>& o)
    {
      _s.merge(o._s);
    }
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    const values_type& mean() const override { return _s.mean(); }
  protected:
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    derivatives(size_t n) : base(), _s(n) {}
    /** 
     * Get the mean of a component 
     * 
     * @f[ \overline{x_i}_w = \frac{\sum_j w_{ij}x_{ij}}{\sum_j w_{ij}}@f]
     *
     * @param i Index 
     *
     * @return @f$\overline{x}_w@f$ 
     */
    value_type mean(size_t i) const { return mean()[i]; }
    /** 
     * Get variances 
     *
     * @return vector of means 
     */
    reals_type var() const { return _s.variance(); }
    /** 
     * Get the weighted variance of a component 
     * 
     * @param i Index 
     *
     * @return @f$\mathrm{Var}[x]_w@f$ 
     */
    real_type var(size_t i) const { return var()[i]; }
    /** 
     * @return the uncertainty on mean of each element 
     */
    reals_type uncer() const  { return _s.sem(); }
    /** 
     * Get the uncertainty on the weighted mean of a component 
     * 
     * @param i Index 
     *
     * @return @f$\sqrt{\mathrm{Var}[\overline{x}_w]}@f$ 
     */
    real_type uncer(size_t i) const { return uncer()[i]; }
    /** 
     * The weighted correlation matrix 
     */
    values_type rho() const { return _s.correlation(); }
    /** 
     * Propagate the uncertainty 
     *
     * @f[ e = \sqrt{d^T C d}\quad,@f]
     *
     * where @f$ d@f$ is the partial derivatives of the estimator with
     * respect to each component
     *
     * @f[ d_i = \frac{\partial v}{\partial x_i}\quad, @f]
     *
     * and @f$ C@f$ is the covariance matrix of the elements 
     *
     * @f[ C_{ij} = \rho_{ij}\delta_i\delta_j\quad.@f]
     *
     * @param df The derivatives
     *
     * @return @f$ e@f$ 
     */
    value_type propagate(const values_type& df) const
    {
      size_t  m = _s.size();
      assert(df.size() == _s.size());

      // Calculate covariance using correlation and uncertainties.  We
      // do this, rather than taking the weighted covariance directly,
      // because we need the uncertainties on the _weighted_ mean, which
      // is _not_ what is stored in the covariance matrix.
      reals_type  u     = uncer();
      reals_type  e     = trait_type::outer(u,u);
      values_type covar = trait_type::mul(e, rho());
      // reals_type  sd    = _s.std();
      // reals_type  den   = trait_type::outer(sd,sd);
      
      // Calculate the matrix-vector product (a vector) C*d 
      values_type cdf(m);
      for (size_t i = 0; i < m; i++)
	for (size_t j = 0; j < m; j++) 
	  cdf[i] += covar[i * m + j] * df[j];
      
      
      // Calculate the vector-vector product (a scalar) d^T*(C*d)
      value_type t = 0;
      t = std::inner_product(std::begin(df),std::end(df),std::begin(cdf),t);
      // if (std::isnan(t) || t < 0) {
      // 	std::cout << "=== t: " << t << " ===" << std::endl;
      // 	pv("u  ", u);
      // 	pm("e  ", e);
      // 	pv("var",_s.variance());
      // 	pm("cov",_s.covariance());
      // 	pm("den",den);
      // 	pm("rho",rho());
      // 	pm("c  ", covar);
      // 	pv("df ",df);
      // 	pv("cdf",cdf);
      // }
	
      return t;
    }
    /** 
     * Calculate derivatives with respect to each input variable. 
     *
     * Derived classes can overload this member function for explicit
     * expression for the derivatives.  The default implementation
     * does a numeric differentiation using a mid-point approximation.
     *
     * @param means Where to evaluate the derivatives. 
     *
     * @returns derivatives with respect to each variable
     */
    virtual values_type gradient(const values_type& means) const
    {
      reals_type  s = _s.std();
      values_type d(means.size());
      values_type dx(0., means.size());
      for (size_t i = 0; i < means.size(); i++) {
	dx[i] = s[i];
	d[i]  = (s[i] > 0 ?
		 (this->value(means+dx)-this->value(means-dx)) / (2*s[i]) :
		 0);
	dx[i] = 0;
      }
      return d;
    }
    /** 
     * Calculate the uncertainty of the measurements using a bootstrap 
     *
     * @return The uncertainty 
     */
    value_type uncertainty(const values_type& means) const override
    {
      values_type df = gradient(means);
      return std::sqrt(propagate(df));
    }
    /** The statistics */
    cov_type _s;
  };
  /** 
   * @example derivatives.cc 
   * @ingroup statest_uncer
   *
   * We use the statest::derivatives method to estimate the
   * uncertainty on the estimator
   *
   * @f[\hat\theta = a+b^2+c^3\quad.@f]
   *
   * Each are distributed as 
   *
   * @f{align*}{
   a &\sim \mathcal{N}[0,1] + 1\\
   b &\sim \frac1{10}\mathcal{N}[0,1]\\ 
   c &\sim \frac1{10}\mathcal{N}[0,1] + a
   @f}
   *
   * Possible output 
   *
   * @verbatim 
      1.935 +/-   0.1208
   @endverbatim
  */
}
#endif
//
// EOF
//


