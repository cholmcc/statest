/**
 * @file   statest/sample_cov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and variance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_SAMPLE_COV_HH
#define STAT_SAMPLE_COV_HH
#include <statest/basic_cov.hh>
#include <statest/sample.hh>
#include <vector>
#include <numeric>

namespace statest
{
  /** 
   * Calculation of means and variances without weights on a full sample. 
   *
   * For a sample 
   *
   * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
   *
   * the mean and variance are given by 
   *
   @f{align*}{
    \overline{x} &= \frac1N\sum_{i=1}^Nx_i
    s^2          &= C\frac1N\sum_{i=1}^N\left(x_i-\overline{x}right)^2\	\
    C            &= \begin{cases} 1 & \text{biased}\\ \frac{N}{N-\delta}
    \end{cases}\quad.
   @f}
   *
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/sample_cov.hh>
   */
  template <typename T=double>
  struct sample_cov : public basic_cov<T>, public sample<T>
  {
    /** Base class */
    using base=basic_cov<T>;
    /** Utility class */
    using util=sample<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;
    /** 
     * Constructor 
     *
     * @param n    size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     */
    sample_cov(size_t n, size_t ddof=0)
      : base(n, ddof), util()
    {}


    /** 
     * @{ 
     * @name Process the sample 
     */
    /** Set the sample.  Suppose the size of the statistic is @f$ M@f$
     * (that many components in each observation), and consist of
     * @f$N@f$ observations, then the sample can given as
     * 
     * @f[\{\{x_{1,1},\ldots,x_{M,1}\},\ldots,
     \{x_{1,N},\ldots,x_{M,N}\}\}\quad.@f]
     *
     * or 
     *
     * @f[\{\{x_{1,1},\ldots,x_{1,N}\},\ldots,\{x_{M,1},\ldots,x_{M,N}\}\}@f]
     *
     * @param values the values of the sample 
     *
     * @return Reference to this object 
     */
    template <template <class ...> class C,
	      template <class ...> class D>
    sample_cov& operator=(const C<D<value_type>>& values)
    {
      return this->operator=(this->flatten(values, size()));
    }
    /** 
     * Assign sample.  If statistic has size @f$ M@f$, then the values
     * are assumed to be in the order
     *
     * @f[\{x_{1,1},\ldots,x_{1,N},\ldots,\{x_{M,1},\ldots,x_{M,N}\}@f]
     *
     * @param x Values of sample 
     */
    sample_cov& operator=(const values_type& x)
    {
      size_t m = size();
      _n       = x.size() / m;

      real_type  c = 1. / (_n - _ddof);

      std::vector<values_type> cache;
      
      for (size_t i = 0; i < m; i++) {
	values_type y = x[std::slice(_n * i, _n, 1)];
	_mean[i]      = y.sum() / _n;
	cache.emplace_back(y-_mean[i]);
      }

      for (size_t i = 0; i < m; i++) {
	for (size_t j = 0; j <= i; j++) {
	  value_type t = 0;
	  _cov[i * m + j] = 
	    c * std::inner_product(std::begin(cache[i]),
				   std::end  (cache[i]),
				   std::begin(cache[j]),t);
	  _cov[j * m + i] = _cov[i * m + j];
	}
      }
      return *this;
    }
    /** 
     * Assign sample.  If statistic has size @f$ M@f$, then the values
     * are assumed to be in the order
     *
     * @f[\{x_{1,1},\ldots,x_{1,N},\ldots,\{x_{M,1},\ldots,x_{M,N}\}@f]
     *
     * @param values Values of sample 
     */
    sample_cov& operator=(const std::initializer_list<value_type>& values)
    {
      values_type x(values);
      return this->operator=(x);
    }
    /** Set the sample.  Suppose the size of the statistic is @f$ M@f$
     * (that many components in each observation), and consist of
     * @f$N@f$ observations, then the sample can given as
     * 
     * @f[\{\{x_{1,1},\ldots,x_{M,1}\},\ldots,
     \{x_{1,N},\ldots,x_{M,N}\}\}\quad.@f]
     *
     * or 
     *
     * @f[\{\{x_{1,1},\ldots,x_{1,N}\},\ldots,\{x_{M,1},\ldots,x_{M,N}\}\}@f]
     *
     * @param values the values of the sample 
     *
     * @return Reference to this object 
     */
    sample_cov& operator=(const std::initializer_list<std::initializer_list<value_type>>& values)
    {
      return this->operator=(this->flatten(values, size()));
    }
    /** @} */
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */    
    sample_cov(sample_cov&& o) : base(std::move(o)) {}
    /** Deleted copy constructor */
    sample_cov(const sample_cov&) = delete;
    /** Deleted assignment operator */
    sample_cov& operator=(const sample_cov&) = delete;
    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Fill in observation - a no-op
     */
    void fill(const values_type&) override {}
    /** 
     * Fill in observation - a no-op 
     */
    void fill(const values_type&, const weights_type&) override { }
    /** 
     * Merge another statistics object into this - a no-op
     *
     * @return Reference to this 
     */
    sample_cov& merge(const sample_cov&) { return *this; }
    /** @} */

    /** 
     * @{ 
     * @name JSON input/output 
     */
    /** 
     * Merge in from JSON object - a no-op 
     */
    void merge_json(const json::JSON&) override {}
    /** @} */
  protected:
    // Clarify scope
    using base::_n;
    // Clarify scope
    using base::_ddof;
    // Clarify scope
    using base::_mean;
    // Clarify scope
    using base::_cov;
    // Clarify scope
    using base::size;

    std::string id() const override { return "sample_cov"; }
  };
  /** 
   * @example sample_cov.cc
   * @ingroup statest_stat
   *
   * Example of calculating the variance from an un-weighted sample. 
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   */
}
#endif
//
// EOF
//

