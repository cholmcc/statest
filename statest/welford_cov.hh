/**
 * @file   statest/welford_cov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and covariance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_SAMPLE_COV_HH
#define STAT_SAMPLE_COV_HH
#include <statest/basic_cov.hh>

namespace statest
{
  /**
   * Online calculation of means and covariance without weights.
   *
   * For a sample 
   *
   * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
   *
   * of multi-valued observations, the mean and variance after @f$n
   * n@f$ observations are given by
   *
   @f{align*}{
   \overline{x}_n &= \overline{x}_{n-1} + \frac{1}n (x_n - \overline{x}_{n-1})\\
   c_n            &= \frac{n-1-\delta}{n}c_{n-1} + \frac{1}{n-\delta}(x_n - \overline{x}_{n-1}) (x - \overline{x}_n)^T\\
   \delta         &= \begin{cases} 0 & \text{biased}\\ 1 \end{cases}\quad.
   @f}
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   *
   * @include examples/welford_cov.cc
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
       |    mean |  uncer. |       0 |       1 |       2 |       3 
   ----+---------+---------+---------+---------+---------+---------
     0 |   -0.05 |    0.09 |    0.90 |    0.07 |   -0.13 |   -0.04 
     1 |   -0.09 |    0.09 |    0.07 |    0.88 |   -0.05 |    0.04 
     2 |   -0.05 |    0.09 |   -0.13 |   -0.05 |    0.83 |    0.01 
     3 |    0.00 |    0.09 |   -0.04 |    0.04 |    0.01 |    0.86 
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * 
   * @ingroup statest_stat
   * @headerfile "" <statest/welford_cov.hh>
   */
  template <typename T=double>
  struct welford_cov : public basic_cov<T>
  {
    /** base type */
    using base=basic_cov<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;

    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    welford_cov(size_t n=3,size_t ddof=0) : base(n,ddof) {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    welford_cov(welford_cov&& o) : base(std::move(o)) {}
    /** Deleted copy constructor */
    welford_cov(const welford_cov&) = delete;
    /** Deleted assignment operator */
    welford_cov& operator=(const welford_cov&) = delete;

    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     */
    void fill(const values_type& x) override
    {
      assert(x.size() == _mean.size());

      if (_n < 0) {
	_mean = x;
	_n    = 1;
	return;
      }
      
      _n++;
      values_type dx =  x - _mean;
      _mean           += value_type(1. / _n) * dx;
      values_type dy =  x - _mean;
      _cov            *= real_type(_n-1) / _n;
      _cov            += value_type(1. / _n) * trait_type::outer(dy,dx);
      _cov            *= real_type(_n) / (_n-_ddof);
    }
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     */
    void fill(const values_type& x, const weights_type&) override
    {
      fill(x);
    }
    /** 
     * Merge another statistics object into this 
     *
     * @param o Other 
     *
     * @return Reference to this 
     */
    welford_cov& merge(const welford_cov& o)
    {
      assert(o.size() == this->size());
      if (_n == 0) {
	_n    = o._n;
	_mean = o._mean;
	_cov  = o._cov;
	return *this;
      }
      if (o._n <= 0) return *this;

      size_t      nn =  _n + o._n;
      real_type   on =  real_type(o._n);
      values_type dx =  o._mean - _mean;
      _mean          += value_type(on / nn) * dx;
      values_type dy =  o._mean - _mean;
      _cov           *= real_type(_n - _ddof) / nn;
      _cov           += value_type(on / (nn - _ddof))*(o._cov+trait_type::outer(dy,dx));
      _n             =  nn;

      return *this;
    }
    /** @} */

    /** 
     * @{ 
     * @name JSON input/output 
     */
    /** 
     * Merge in from JSON object 
     */
    void merge_json(const json::JSON& json) override
    {
      welford_cov<T> w(this->size());
      w.from_json(json);
      merge(w);
    }
    /** @} */
  protected:
    /** claify scope */
    using base::_mean;
    /** claify scope */
    using base::_n;
    /** claify scope */
    using base::_ddof;
    /** claify scope */
    using base::_cov;

    std::string id() const override { return "welford_cov"; }
  };
  /** 
   * @example welford_cov.cc
   * @ingroup statest_stat
   *
   * Example of calculating the covariance matrix from an un-weighted
   * sample.
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   */
}
#endif
//
// EOF
//

