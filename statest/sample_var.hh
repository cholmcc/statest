/**
 * @file   statest/sample_var.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and variance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_SAMPLE_VAR_HH
#define STAT_SAMPLE_VAR_HH
#include <statest/basic_var.hh>
#include <statest/sample.hh>

namespace statest
{
  /** 
   * Calculation of means and variances without weights on a full sample. 
   *
   * For a sample 
   *
   * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
   *
   * the mean and variance are given by 
   *
   @f{align*}{
    \overline{x} &= \frac1N\sum_{i=1}^Nx_i
    s^2          &= C\frac1N\sum_{i=1}^N\left(x_i-\overline{x}right)^2\\
    C            &= \begin{cases} 1 & \text{biased}\\ \frac{N}{N-\delta}
    \end{cases}\quad.
   @f}
   *
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/sample_var.hh>
   */
  template <typename T=double>
  struct sample_var : public basic_var<T>, public sample<T>
  {
    /** Base class */
    using base=basic_var<T>;
    /** Utility base class */
    using util=sample<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;
    /** 
     * Constructor 
     *
     * @param n    size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     * @param diff If true, calculate variance by difference (not residuals)
     */
    sample_var(size_t n, size_t ddof=0, bool diff=false)
      : base(n, ddof), util(), _diff(diff)
    {}

    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */    
    sample_var(sample_var&& o) : base(std::move(o)), util() {}
    /** Deleted copy constructor */
    sample_var(const sample_var&) = delete;
    /** Deleted assignment operator */
    sample_var& operator=(const sample_var&) = delete;
    /** 
     * @{ 
     * @name Process the sample 
     */
    /** Set the sample.  Suppose the size of the statistic is @f$ M@f$
     * (that many components in each observation), and consist of
     * @f$N@f$ observations, then the sample can given as
     * 
     * @f[\{\{x_{1,1},\ldots,x_{M,1}\},\ldots,
     \{x_{1,N},\ldots,x_{M,N}\}\}\quad.@f]
     *
     * or 
     *
     * @f[\{\{x_{1,1},\ldots,x_{1,N}\},\ldots,\{x_{M,1},\ldots,x_{M,N}\}\}@f]
     */
    template <template <class ...> class C,
	      template <class ...> class D>
    sample_var& operator=(const C<D<value_type>>& values)
    {
      return this->operator=(this->flatten(values, size()));
    }
    /** 
     * Assign sample.  If statistic has size @f$ M@f$, then the values
     * are assumed to be in the order
     *
     * @f[\{x_{1,1},\ldots,x_{1,N},\ldots,\{x_{M,1},\ldots,x_{M,N}\}@f]
     *
     */
    sample_var& operator=(const values_type& x)
    {
      size_t     m = size();
      _n           = x.size() / m;
      real_type  c = 1. / (_n - _ddof);

      for (size_t i = 0; i < m; i++) {
	values_type y = x[std::slice(i*_n, _n, 1)];
	_mean[i]      = y.sum() / _n;
	if (!_diff) 
	  _var[i]     = c*std::pow(y-_mean[i],2).sum();
	else {
	  auto m2     = std::pow(y,2).sum() / _n;
	  _var[i]     = _n * c * (m2 - _mean[i] * _mean[i]);
	}
      }
      return *this;
    }
    /** Set the sample.  Suppose the size of the statistic is @f$ M@f$
     * (that many components in each observation), and consist of
     * @f$N@f$ observations, then the sample can given as
     * 
     * @f[\{\{x_{1,1},\ldots,x_{M,1}\},\ldots,
     \{x_{1,N},\ldots,x_{M,N}\}\}\quad.@f]
     *
     * or 
     *
     * @f[\{\{x_{1,1},\ldots,x_{1,N}\},\ldots,\{x_{M,1},\ldots,x_{M,N}\}\}@f]
     *
     * @param values the values of the sample 
     *
     * @return Reference to this object 
     */
    sample_var& operator=(const std::initializer_list<std::initializer_list<value_type>>& values)
    {
      return this->operator=(this->flatten(values, size()));
    }
    /** 
     * Assign sample.  If statistic has size @f$ M@f$, then the values
     * are assumed to be in the order
     *
     * @f[\{x_{1,1},\ldots,x_{1,N},\ldots,\{x_{M,1},\ldots,x_{M,N}\}@f]
     *
     * @param values Values of sample 
     */
    sample_var& operator=(const std::initializer_list<value_type>& values)
    {
      values_type x(values);
      return this->operator=(x);
    }
    /** @} */
    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Fill in observation - a no-op
     */
    void fill(const values_type&) override {}
    /** 
     * Fill in observation - a no-op 
     */
    void fill(const values_type&, const weights_type&) override { }
    /** 
     * Merge another statistics object into this - a no-op
     *
     * @return Reference to this 
     */
    sample_var& merge(const sample_var&) { return *this; }
    /** @} */

    /** 
     * @{ 
     * @name JSON input/output 
     */
    /** 
     * Merge in from JSON object - a no-op 
     */
    void merge_json(const json::JSON&) override {}
    /** @} */
  protected:
    // Clarify scope
    using base::_n;
    // Clarify scope
    using base::size;
    // Clarify scope
    using base::_ddof;
    // Clarify scope
    using base::_mean;
    // Clarify scope
    using base::_var;


    std::string id() const override { return "sample_var"; }

    /** If true, calculate variance by difference */
    bool _diff; 
  };
  /** 
   * @example sample_var.cc
   * @ingroup statest_stat
   *
   * Example of calculating the variance from an un-weighted sample. 
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   */
}
#endif
//
// EOF
//

