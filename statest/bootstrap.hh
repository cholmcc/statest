/**
 * @file   statest/bootstrap.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_BOOTSTRAP_HH
#define CORRELATIONS_STAT_BOOTSTRAP_HH
#include <statest/sub_samples.hh>

namespace statest
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using bootstrap estimate of uncertainties.  
   * 
   * Note that this method generally overestimates the uncertainties.
   *
   * See the example @ref bootstrap.cc 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_uncer
   * @headerfile "" <statest/bootstrap.hh>
   */
  template<typename T=double,unsigned short NSUB=10,unsigned short NSIM=1000>
  struct bootstrap : public sub_samples<T,NSUB>
  {
    /** Base class */
    using base=sub_samples<T>;
    /** Value value type */
    using value_type=typename base::value_type;

    /** Destructor */
    virtual ~bootstrap() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    bootstrap(bootstrap&& o) : base(std::move(o)) {}
    /** 
     * Normal copy constructor (deleted)
     */
    bootstrap(const bootstrap&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    bootstrap& operator=(const bootstrap&) = delete;
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a o
     */
    std::ostream& print(std::ostream& o, unsigned int f=0x0) const override
    {
      size_t w = o.width(0);
      if (f >= estimator<T>::MORE) 
	o << std::setw(0) << "T=" << NSIM << " ";

      o  << std::setw(w);
      return base::print(o, f);
    }
  protected:
    /** Statistics calculator for weighted mean and covariance */
    using var_type=west_var<T>;
    /** Clarify scope */
    using base::_sub;
    /** clarify scope */
    using base::_s;
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    bootstrap(size_t n)
      : base(n)
    {}
    /** 
     * Calculate the uncertainty of the measurements using a bootstrap 
     *
     * @return The uncertainty 
     */
    value_type simulate() const override
    {
      var_type s(1);
      for (size_t i = 0; i < NSIM; i++) {
	size_t     k = this->select();
	value_type r = this->value(this->_sub[k].mean());
	s.fill({r},{1.});
      }
      return s.std()[0];
    }
  };
  /** 
   * @example bootstrap.cc 
   * @ingroup statest_uncer
   *
   * We use the statest::bootstrap method to estimate the uncertainty
   * on the estimator 
   *
   * @f[\hat\theta = a+b^2+c^3\quad.@f]
   *
   * Each are distributed as 
   *
   * @f{align*}{
   a &\sim \mathcal{N}[0,1] + 1\\
   b &\sim \frac1{10}\mathcal{N}[0,1]\\ 
   c &\sim \frac1{10}\mathcal{N}[0,1] + a
   @f}
   *
   * Possible output 
   *
   * @verbatim 
      2.064 +/-   0.3793
   @endverbatim
  */
}
#endif
//
// EOF
//

  
