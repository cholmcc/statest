/**
 * @file   statest/west_var.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief West update for mean and variance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WEST_VAR_HH
#define STAT_WEST_VAR_HH
#include <statest/basic_var.hh>
#include <statest/west.hh>

namespace statest
{
  /** 
   * Online calculation of means and variances with component,
   * non-frequency weights.
   *
   *
   * For a weighted sample 
   *
   * @f[X=\left\{(x_1,w_1),\ldots,(x_N,w_N)\right\}\quad,@f] 
   *
   * the mean and variance is given by 
   *
   * @f{align*}{
   W_{1,n}        &= W_{1,n-1} + w\\
   W_{2,n}        &= W_{2,n-1} + w^2\\ 
   \overline{x}_n &= \overline{x}_{n-1} + \frac{w}{W_{1,n}} (x - \overline{x}_{n-1})\\
   s^2_{x,n}      &= \frac{\Delta_{n-1}}{\Delta_{n}} s^2_{x,n-1}  + \frac{w}{\Delta_{n}} (x - \overline{x}_{n-1}) (x - \overline{x}_n)\\
   \Delta_n       &= W_{1,n} -  \begin{cases}
                       0 & \text{biased, frequency weights}\\
                       1 & \text{frequency weights}\\
                       \frac{W_{2,n}}{W_{1,n}}\\
                     \end{cases}\quad.
   @f}
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   * - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Weighted_incremental_algorithm
   * 
   * @include examples/west_var.cc
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean | variance |      std |   uncer.
   -----+----------+----------+----------+----------
      0 |     0.07 |     0.90 |     0.95 |     0.10
      1 |    -0.19 |     0.78 |     0.88 |     0.09
      2 |    -0.09 |     0.92 |     0.96 |     0.10
      3 |     0.18 |     1.10 |     1.05 |     0.11
   @endverbatim
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/west_var.hh>
   */
  template <typename T=double>
  struct west_var : public basic_var<T>, public west<T>
  {
    /** base type */
    using base=basic_var<T>;
    /** Utility type */
    using util=west<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;
    /** 
     * Constructor 
     *
     * @param n    size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     */
    west_var(size_t n, size_t ddof=0)
      : base(n, ddof),
	_sumw(n),
	_sumw2(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    west_var(west_var&& o)
      : base(std::move(o)),
	_sumw(std::move(o._sumw)),
	_sumw2(std::move(o._sumw2))
    {}
    /** Deleted copy constructor */
    west_var(const west_var&) = delete;
    /** Deleted assignment operator */
    west_var& operator=(const west_var&) = delete;

    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Reset state 
     */
    virtual void reset() override
    {
      base::reset();
      std::fill(std::begin(_sumw),  std::end(_sumw),  0);
      std::fill(std::begin(_sumw2), std::end(_sumw2), 0);
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     * @param w  weights 
     */
    void fill(const values_type& x, const weights_type& w) override 
    {
      _fill(w,w*w,x,value_type(0),1);
    }
    /** 
     * Fill in observation 
     *
     * @param z  Observations 
     */
    void fill(const values_type& z) override 
    {
      const weights_type w(1,z.size());
      fill(z,w);
    }
    /** 
     * Merge other statistics object into this 
     *
     * @param o Other object to merge in 
     *
     * @return Reference to this 
     */
    west_var& merge(const west_var& o)
    {
      assert(o.size() == this->size());
      if (o._n <= 0) return *this;
      _fill(o._sumw,o._sumw2,o._mean,o._var,o._n);
      return *this;
    }
    /** @} */

    /** 
     * @{ 
     * @name Get information 
     */
    /** @return standard uncertainty on the mean */
    reals_type sem() const override 
    {
      return std() / std::sqrt(_sumw);
    }
    /** Clarify scope */
    using base::std;
    /** @} */
    
    /** 
     * @{ 
     * @name JSON input/output 
     */
    /** 
     * Merge in from JSON object 
     */
    void merge_json(const json::JSON& json) override 
    {
      west_var<T> w(this->size());
      w.from_json(json);
      merge(w);
    }
    // Clarify scope
    using base::to_json_array;
    using base::from_json_array;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON to_json() const override
    {
      json::JSON json = base::to_json();
      json["sumw"]  = to_json_array(_sumw);
      json["sumw2"] = to_json_array(_sumw2);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    void from_json(const json::JSON& json) override 
    {
      base::from_json(json);
      from_json_array(json["sumw"],_sumw);
      from_json_array(json["sumw2"],_sumw2);
    }
    /** @} */
  protected:
    /** Clarify scope */
    using base::_n;
    /** Clarify scope */
    using base::_ddof;
    /** Clarify scope */
    using base::_mean;
    /** Clarify scope */
    using base::_var;
    /** Clarify scope */
    using util::denom;

    std::string id() const override { return "west_var"; }

    template <typename C>
    void _fill(const weights_type& sumw,
	       const weights_type& sumw2,
	       const values_type&  mean,
	       const C&            var,
	       size_t              n)
    {
      if (_n == 0) {
	_mean  = mean;
	_var   = var;
	_sumw  = sumw;
	_sumw2 = sumw2;
	_n     = n;
	return;
      }
      values_type  fac =  denom(_sumw, _sumw2, _ddof);//old sumw
      _sumw             += sumw;
      _sumw2            += sumw2;
      values_type  dx  =  mean - _mean;
      _mean             += trait_type::div(trait_type::mul(sumw,dx), _sumw);
      values_type  dy  =  trait_type::conj(mean - _mean);
      values_type  dxy =  dx*dy;
      values_type  div =  denom(_sumw, _sumw2, _ddof); // new sumw
      _var              *= trait_type::div(fac, div, value_type(1));//old / new
      _var              += trait_type::div(trait_type::mul(sumw,var+dxy),div);
      _n                += n;
    }      

    /** Sum of component weights */
    weights_type _sumw;
    /** Sum of squared component weights */
    weights_type _sumw2;
  };
  /** 
   * @example west_var.cc
   * @ingroup statest_stat
   *
   * Example of calculating the variance from a weighted sample. 
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   * The corresponding four weights are drawn from a uniform
   * distribution
   *
   * @f[ w_i \sim \mathcal{U}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   *
   */
}
#endif
//
// EOF
//

