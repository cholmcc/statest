/**
 * @file   statest/west.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Utilities used by West updates
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WEST_HH
#define STAT_WEST_HH
#include <statest/basic_stat.hh>

namespace statest
{
  /** 
   * Utilities used for West (weighted) updates.  This assumes
   * component, non-frequency weights.
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/West.hh>
   */
  template <typename T=double>
  struct west
  {
    /** value type */
    using value_type=typename basic_stat<T>::value_type;
    /** value vector type */
    using values_type=typename basic_stat<T>::values_type;
    /** Type of weights */
    using weight_type=typename basic_stat<T>::weight_type;
    /** Type of weights */
    using weights_type=typename basic_stat<T>::weights_type;
    /** trait type */
    using trait_type=typename basic_stat<T>::trait_type;

    /** Destructor */
    virtual ~west() {}
  protected:
    /** Constructor - does nothing */
    west() {}
    /** Move constructor - does nothing */
    west(west&&) {}
    /** Deleted copy constructor */
    west(const west&) = delete;
    /** Deleted assignment operator */
    west& operator=(const west&) = delete;
    /** 
     * Calculate the denominator for updates 
     *
     * @f[ D = W - \Delta_{\nu} \frac{W^2}{W}\quad, @f]
     *
     * where @f$ W@f$ is the sum of weights  
     *
     * @f[ W = \sum_i w_iw_i^{T}\quad, @f]
     *
     * @f$ W^2@f$ is the sum of square weights 
     *
     * @f[ W^2 = \sum_i \left(w_iw_i^{T}\right)^2\quad, @f]
     *
     * and @f$\Delta_{\nu}@f$ is the change for the number degrees of
     * freedom (1 for unbiased estimator of the covariance)
     *
     * @param sumW @f$ W@f$ 
     * @param sumW2 @f$ W^2@f$ 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction to @f$\nu@f$ 
     *
     * @return @f$ D@f$ 
     */ 
    values_type denom(const weights_type& sumW,
		      const weights_type& sumW2,
		      size_t              ddof) const
    {
      values_type s(sumW.size());
      for (size_t i = 0; i < sumW.size(); i++) s[i] = sumW[i];
      
      if (ddof == 0) return s;

      if (sumW.apply([](weight_type w) {return w*w;}).sum() < 1e-6)
	return values_type(sumW.size());
    
      return s - value_type(ddof) * trait_type::div(sumW2, sumW, weight_type());
    }
    weights_type scor(const weights_type& sumW,
		       const weights_type& sumW2) const
    {
      return (sumW2 / (sumW*sumW))
	.apply([](weight_type x) { return (x>1e-9 ? std::sqrt(x) : 0); });
    }
  };
}
#endif
//
// EOF
//

