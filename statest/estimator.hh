/**
 * @file   statest/estimator.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_ESTIMATOR_HH
#define CORRELATIONS_STAT_ESTIMATOR_HH
#include <statest/basic_stat.hh>
#include <iostream>
#include <iomanip>

/** 
 * @defgroup statest_uncer Uncertainty estimates 
 * @ingroup statest
 *
 * In this module, we define a number of classes to estimate the
 * statistical uncertainty on some estimator of a statistic on means.
 *
 * @section statest_uncer_deriv Regular error propagation 
 *
 * Suppose we have an statistic @f$f@f$ which is a function of the
 * means of some random variables.
 *
 * @f[f:\overline{x}\mapsto y\quad.@f]
 *
 * Then, the square uncertainty on the estimator of that statistic @f$
 * \hat f@f$ is the variance of the estimator and can be calculated as
 *
 * @f[\mathrm{var}(\hat f) = 
 \sum_{i=1}^{N} \left(\frac{\partial\hat f}{\partial x_i}\delta x_i\right)^2 + 2\sum_{i=1}^{N}\sum_{j=1}^{i-1}\frac{\partial\hat f}{\partial x_i}\frac{\partial \hat f}{\partial x_j}\delta x_i\delta x_j\rho_{ij}\quad,@f]
 * 
 * where 
 *
 * * @f$\partial\hat f/\partial x_i@f$ is the derivative of the
 *   estimator with respect to the mean of the @f$ i^{\text{th}}@f$
 *   random variable,
 *
 * * @f$\delta x_i@f$ is the uncertainty on the mean of the
 *   @f$i^{\text{th}}@f$ random variable, and
 *
 * * @f$\rho_{ij}@f$ is the correlation between the
 *   @f$i^{\text{th}}@f$ and @f$ j^{\text{th}}@f$ random variables
 *
 * In matrix notation we can write 
 *
 * @f[\mathrm{var} = \nabla\hat f^T C \nabla\hat f\quad,@f] 
 *
 * where 
 *
 * * @f$\nabla\hat f@f$ is the gradient of the estimator with respect
 *   to the components (and @f$\nabla\hat f^T@f$ its transpose - or
 *   generally conjugate).
 *
 * * @f$C@f$ is the covariance matrix of the $x$ given by 
 *
 *   @f[C_{ij} = \rho_{ij}\delta x_i\delta x_j\quad.@f]
 * 
 * The above formulas is used in the implementation of the uncertainty
 * estimator class statest::derivates.  The user may specify exact
 * derivatives or the derivatives can be calculated numerically using
 * the expression for the estimator.
 *
 * In some cases, however, it may not be straight forward to calculate
 * the derivatives or the exact covariance (correlation) cannot easily
 * be calculated.  In these cases we may estimate the variance of the
 * estimator by the use of the _bootstrap_ or _jackknife_ algorithms.
 *
 * @subsection statest_uncer_note Note
 * This is _not_ the same as estimating the uncertainty on the
 * estimator of a statistic defined on the sample.  Suppose we have
 * the sample
 *
 * @f[Y=\{y_1,\ldots,y_N\}\quad,@f] 
 *
 * and the estimator 
 *
 * @f[\hat g:y_i\mapsto z_i\quad,@f] 
 *
 * then the uncertainty is given by the variance of the estimator
 * evaluated over the sample
 *
 * @f[\mathrm{var}(\hat g) = 
 C\frac{1}{N}\sum_{i=1}^N \left(g(y_i)-\overline{g(Y)}\right)^2\quad,
 @f]
 *
 * with 
 *
 * @f[\overline{g(Y)}=\frac1N\sum_{i=1}^N g(y_i)\quad,@f]
 * 
 * and @f$C@f$ being Bessel's correction.  That is, in _this_ case one
 * does not need the services of the classes in this module - one can
 * rather use the classes in the @ref statest_stat module directly.
 *
 * @section statest_uncer_boot Bootstrap estimate 
 *
 * Suppose we have an estimator @f$\hat f@f$ as above which we
 * evaluate over some sample means
 *
 * @f[ X=\{\overline{x}_1,\ldots,\overline{x}_N\}\quad.@f]
 *
 * We may not know the exact distribution function for the
 * observations @f$x_i@f$, but we may simulate it by drawing $N$
 * observations (with replacement) from our sample
 *
 * @f[ X^*_i =
 * \{\overline{x}^*_1,\ldots,\overline{x}^*_N|\overline{x}^*_i\sim
 * X\}\quad.@f]
 *
 * We can then calculate the estimator @f$ \hat f@f$ on this simulated
 * sample
 * 
 * @f[ f^*_i = \hat f(X^*_i)\quad.@f]
 * 
 * If we repeat this simulation @f$ M@f$ times, we can estimate the
 * variance of the estimator @f$\hat f@f$ as
 *
 * @f[\mathrm{var}_{\mathrm{bootstrap}}(\hat f) = 
 \mathrm{var}(f^*) = 
 \frac1M\sum_{i=1}^{M}\left(f^*_i - \overline{f^*}\right)^2\quad.@f]
 *
 * The class statest::boostrap implements this algorithm for
 * estimating the variance of a statistic.
 *
 * @section statest_uncer_jack Jackknife estimate 
 *
 * A similar, but simpler, approach to estimating the variance of an
 * estimator of statistic @f$\hat f@f$ is the _jackknife_ algorithm.
 * Again, the algorithm is based on simulating the distribution
 * function from samples drawn from the original sample.  However, in
 * this method, we calculate the statistic on the original sample
 * leaving one observation out, and then calculate the variance over
 * this simulations where each observation is left out.
 *
 * That is, if we have 
 *
 * @f[X=\{\overline{x}_1,\ldots,\overline{x}_N\}\quad,@f] 
 *
 * estimates of the mean, we denote 
 *
 * @f[X^*_i = \{\overline{x}_j|j=1,\ldots,i-1,i+1,\ldots,N\}\quad,@f] 
 *
 * the set of estimates leaving out the @f$i^{\text{th}}@f$ estimate, and 
 *
 * @f[f^*_i = \hat f(X^*)\quad,@f] 
 *
 * is our estimator of the statistics @f$ f@f$ evaluated on this
 * sample.  The estimate of the variance is then given by
 *
 * @f[\mathrm{var}_{\mathrm{jackknife}}(\hat f) 
 = \frac{N-1}{N}\sum_{i=1}^N\left(f^*_i - \overline{f^*}\right)^2\quad.@f]
 *
 * The class statest::jackknife implements this algorithm for
 * estimating the variance of a statistic.
 *
 */
namespace statest
{
  /** 
   * Base class for an estimator.  Sub-classes implements specific
   * ways of estimating the uncertainty on the estimator.  Defined
   * subclasse are
   *
   * - derivatives - full uncertainty propagation via covariance 
   * - bootstrap - Simulated estimate using bootstrapping 
   * - jackknife - Simulated estimate using jackknife prescription 
   *
   * In the example below we use these three methods to estimate the
   * uncertainty of
   *
   * @f[\theta = e^{\mu}\quad,@f] 
   * 
   * for the sample of 100 observations of @f$
   * X\sim\mathcal{N}[5,0]@f$.  We use the estimator
   *
   * @f[\hat\theta = e^{\bar X}\quad.@f] 
   *
   * We use the three methods above.  Note, for the `derivatives`
   * method we rely on numerical differentiation. The example is from
   * L.Wasserman [All of
   * Statistics](http://www.stat.cmu.edu/~larry/all-of-statistics/),
   * Chapter 8.
   *
   * @include examples/estimator.cc
   *
   * Possible output 
   *
   * @verbatim 
    derivatives:   123.5 +/-     14.6
      bootstrap:   123.5 +/-    46.76
      jackknife:   123.5 +/-    22.39
   @endverbatim
   *
   * @tparam T The value type 
   * @ingroup statest_uncer
   */
  template <typename T=double>
  struct estimator
  {
    enum {
      MORE = 0x1,
      DETAILS = 0x2
    };
    /** Statistics base class */
    using base=basic_stat<T>;
    /** traits */
    using trait_type=typename base::trait_type;
    /** real value type */
    using value_type=typename base::value_type;
    /** A value vector */
    using values_type=typename base::values_type;
    /** real weight type */
    using weight_type=typename base::weight_type;
    /** A weight vector */
    using weights_type=typename base::weights_type;
    /** real real type */
    using real_type=typename base::real_type;
    /** A real vector */
    using reals_type=typename base::reals_type;
    /** result */
    using result_type=std::pair<value_type,value_type>;

    /**
     * Destructor 
     */
    virtual ~estimator() {}
    /** 
     * Move copy constructor 
     */
    estimator(estimator&&) {}
    /** 
     * Normal copy constructor (deleted)
     */
    estimator(const estimator&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    estimator& operator=(const estimator&) = delete;
    /** 
     * Calculate the final result and uncertainty 
     *
     * @return result and uncertainty 
     */
    virtual result_type eval() const
    {
      value_type val = value(mean());
      value_type unc = uncertainty(mean());
      return std::make_pair(val,unc);
    }
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  weights 
     */
    virtual void fill(const values_type& x, const weights_type& w) = 0;
    /** 
     * Number of input parameters expected 
     */
    virtual size_t size() const = 0;
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     *
     * @return The output stream @a o
     */
    virtual std::ostream& print(std::ostream& o, unsigned int =0x0) const
    {
      size_t w = o.width(0);

      auto res = eval();
      return o << std::setw(w) << res.first << " +/- "
	       << std::setw(w) << res.second;
    }
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    virtual json::JSON to_json() const = 0;
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    virtual void from_json(const json::JSON& json) = 0;
    /**
     * Merge from JSON 
     *
     * @param json 
     */
    virtual void merge_json(const json::JSON& json) = 0;
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    virtual const values_type& mean() const = 0;
  protected:
    /** Constructor */
    estimator() {}
    /** 
     * Virtual interface for calculating the estimator 
     *
     * @param means Means of each component 
     *
     * @return The estimator evaluated at the means 
     */
    virtual value_type value(const values_type& means) const = 0;
    /** 
     * Calculate the uncertainty of the estimator 
     *
     * @return The uncertainty 
     */
    virtual value_type uncertainty(const values_type& means) const = 0;
  };
  /** 
   * @example estimator.cc 
   * @ingroup statest_uncer
   *
   * An example of an estimator. We use the three methods
   * (statest::derivatives, statest::bootstrap, and
   * statest::jackknife) to estimate the uncertainty of
   *
   * @f[\theta = e^{\mu}\quad,@f] 
   * 
   * for the sample of 100 observations of
   * @f$X\sim\mathcal{N}[5,0]@f$.  We use the estimator
   *
   * @f[\hat\theta = e^{\bar X}\quad.@f] 
   *
   * Note, for the `derivatives` method we rely on numerical
   * differentiation. The example is from L.Wasserman [All of
   * Statistics](http://www.stat.cmu.edu/~larry/all-of-statistics/),
   * Chapter 8.
   *
   */
  /** 
   * @example test_estimator.cc 
   * @ingroup statest_uncer
   *
   * @test Test of estimator classes 
   */
}
#endif
//
// EOF
//

    
    
   
    
    
