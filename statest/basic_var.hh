/**
 * @file   statest/basic_var.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief On-line calculations of means and variances 
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_VAR_HH
#define CORRELATIONS_STAT_VAR_HH
#include <statest/basic_stat.hh>

/** 
 * Namespace for statistics code.
 *
 * @ingroup statest
 */
namespace statest
{
  //====================================================================
  /** 
   * base class for calculating means and variances 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat 
   * @headerfile "" <statest/basic_var.hh>
   */
  template <typename T=double>
  struct basic_var : public basic_stat<T>
  {
    /** Base type */
    using base=basic_stat<T>;
    /** Trait type */
    using trait_type=typename base::trait_type;
    /** Value type */
    using value_type=typename base::value_type;
    /** Value vector type */
    using values_type=typename base::values_type;
    /** Weight type */
    using weight_type=typename base::weight_type;
    /** Weight vector type */
    using weights_type=typename base::weights_type;
    /** Real type */
    using real_type=typename base::real_type;
    /** Real vector type */
    using reals_type=typename base::reals_type;
    
    /** Destructor */
    virtual ~basic_var() {}

    /** 
     * @name Manipulations 
     * @{ 
     */
    /** Reset state */
    virtual void reset() override
    {
      base::reset();
      std::fill(std::begin(_var), std::end(_var), 0);
    }
    /** @} */

    /** 
     * @name Get information 
     * @{ 
     */
    /** @return the variance */
    reals_type variance() const override { return trait_type::real(_var); }

    // Clarify scope 
    using base::std;

    // Clarify scope 
    using base::sem;
    /** @} */

    /** 
     * @name Show state 
     * @{ 
     */
    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags
     *
     * @return @a o 
     */
    std::ostream& print(std::ostream& o=std::cout, unsigned int f=0x1) const override
    {
      size_t w = o.width(0);
      o << "n=" << _n << "\t(ddof=" << _ddof << ")\n";

      if (f < 1) return o;

      w        = 10;
      size_t n = _mean.size();
      
      o << "     | "
	<< std::setw(w) << "mean"     << " | "
	<< std::setw(w) << "variance" << " | "
	<< std::setw(w) << "std"      << " | "
	<< std::setw(w) << "uncer."   << "\n";

      auto& m = _mean;
      auto& v = _var;
      auto  s = std();
      auto  e = sem();
      o << "-----";
      for (size_t i = 0; i < 4; i++) {
	o << "+-";
	for (size_t j = 0; j < w+1; j++) o << '-';
      }
      o << '\n';
      for (size_t i = 0; i < n; i++) 
	o << std::setw(4) << i << " | "
	  << std::setw(w) << m[i] << " | "
	  << std::setw(w) << v[i] << " | "
	  << std::setw(w) << s[i] << " | "
	  << std::setw(w) << e[i] << std::endl;

      return o;
    }
    /** @} */

    /** 
     * @{
     * @name JSON input/output 
     */
    // Clarify scope
    using base::to_json_array;
    // Clarify scope
    using base::from_json_array;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON to_json() const override
    {
      json::JSON json = base::to_json();
      json["var"] = to_json_array(_var);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    virtual void from_json(const json::JSON& json)
    {
      base::from_json(json);
      from_json_array(json["var"],_var);
    }
    /** @} */
    
  protected:
    // Clarify scope
    using base::_n;
    // Clarify scope
    using base::_mean;
    // Clarify scope
    using base::_ddof;
    /** 
     * Constructor 
     *
     * @param n    size of statistics 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction to @f$\nu@f$ 
     */
    basic_var(size_t n, size_t ddof=0)
      : base(n, ddof),
	_var(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    basic_var(basic_var&& o) : base(std::move(o)), _var(std::move(o._var)) {}
    /** Deleted copy constructor */
    basic_var(const basic_var&) = delete;
    /** Deleted assignment operator */
    basic_var& operator=(const basic_var&) = delete;


    /** Variances */
    values_type _var;
  };
  /** 
   * @example catastrophic.cc
   * @ingroup statest_stat
   *
   * Example showing catastrophic cancellation. 
   *
   * In this example, we _numerically_ investigate a sample 
   *
   * @f[ X=\{x_1,\ldots,x_N\}\quad.@f] 
   *
   * We calculate the mean and variance using standard formulas 
   *
   * 
   @f{align*}{
   \overline{x} &= \frac{1}{N}\sum_{i=1}^N x_i\\
   \mathrm{var}(x) 
   &= C\frac1N\sum_{i=1}^N\left(x_i-\overline{x}\right)^2 
   = 
   C\left(\overline{x^2}-\overline{x}^2\right)\quad,
   @f}
   * where 
   *
   *
   * @f[C=\frac{N}{N-\delta}\quad,@f]
   *
   * is Bessel's correction (typically @f$\delta=1@f$ for an unbiased
   * estimator of the population variance).  Note, we use _both_
   * expression for the variance and calculate these numerically.  At
   * the same time we calculate the mean and variance using the
   * statest::welford_var algorithm.
   *
   * We note that for any sample
   * 
   * @f[Y=\{x_i+k|i=1,\ldots,N\}\quad,@f]
   *
   * that 
   *
   * @f{align*}{
   \overline{y}&=\overline{x}+k\\
   \mathrm{var}(y)&=\mathrm{var}(x)\quad,@f}
   *
   * 
   * Indeed, if @f$k@f$ is small or if we use the first expression for
   * the variance or Welford's algorithm, we find the expected
   * results.  However, if @f$k@f$ is large (say @f$10^8@f$) _and_ we
   * use the second expression for the variance
   *
   @f{align*}{ 
   \mathrm{var}(x) =
   C\left(\overline{x^2}-\overline{x}^2\right)\quad, 
   @f}
   *
   * we find that the variance is wrong. 
   *
   * Concretely we pick the samples 
   *
   * @f{align*}{A&=\{4,7,13,16\}\\B&=\{x+10^8|x\in A\}\quad,@f}
   *
   * and calculate the mean and variance (using both expressions).  We
   * find for sample @f$A@f$ that
   * 
   * @f{align*}{
   \overline{x} &= 10 &&\mathrm{var}(x) &= 22.5\quad,@f}
   *
   * while for the shifted sample @f$ B@f$
   *
   * @f{align*}{
   \overline{x} &= 10 &\mathrm{var}(x) &=
   \begin{cases} 22.5 & \text{First expr., or Welford}\\
   22 & \text{Second expr.}
   \end{cases}\quad,@f}
   *
   * illustrating _catastrophic cancellation_ (subtracting two very
   * large numbers).
   *
   * This means that we cannot _reliably_ use the second expression
   * for the variance to calculate it in a single pass over the
   * sample.  Fortunately, Welford's algorithm provides us with a way
   * out of that predicament.
   *
   */
}
#endif
//
// EOF
//
