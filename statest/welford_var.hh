/**
 * @file   statest/welford_var.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and variance
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WELFORDVAR_HH
#define STAT_WELFORDVAR_HH
#include <statest/basic_var.hh>

namespace statest
{
  /** 
   * Online calculation of means and variances without weights.
   *
   * For a sample 
   *
   * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
   *
   * the mean and variance after @f$n n@f$ observations are given by 
   *
   @f{align*}{
    \overline{x}_n &= \overline{x}_{n-1} + \frac{1}{n}(x_n-\overline{x}_{n-1}) \\
    s^2_{n}      &= \frac{n-1-\delta}{n}s^2_{n-1} + \frac{1}{n-\delta}(x_n-\overline{x}_{n-1}) (x_n - \overline{x}_n)\\
    \delta         &= n - \begin{cases} 0 & \text{biased}\\ 1 \end{cases}\quad.
   @f}
   *
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   * - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
   *
   * @include examples/welford_var.cc
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean | variance |      std |   uncer.
   -----+----------+----------+----------+----------
      0 |    -0.05 |     0.90 |     0.95 |     0.09
      1 |    -0.09 |     0.88 |     0.94 |     0.09
      2 |    -0.05 |     0.83 |     0.91 |     0.09
      3 |     0.00 |     0.86 |     0.93 |     0.09
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat
   * @headerfile "" <statest/welford_var.hh>
   */
  template <typename T=double>
  struct welford_var : public basic_var<T>
  {
    using base=basic_var<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;
    /** 
     * Constructor 
     *
     * @param n    size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     */
    welford_var(size_t n, size_t ddof=0) : base(n, ddof) {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */    
    welford_var(welford_var&& o) : base(std::move(o)) {}
    /** Deleted copy constructor */
    welford_var(const welford_var&) = delete;
    /** Deleted assignment operator */
    welford_var& operator=(const welford_var&) = delete;
    /** 
     * @{ 
     * @name Manipulations 
     */
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    void fill(const values_type& x) override
    {
      // assert(x.size() == _mean.size());
    
      if (_n < 0) {
	_mean = x;
	_n    = 1;
	return;
      }
      _n++;
      values_type dx =  x - _mean;
      _mean          += value_type(1. / _n) * dx;
      _var           *= real_type(_n - 1) / _n;
      _var           += value_type(1. / _n) * dx * trait_type::conj(x-_mean);
      _var           *= real_type(_n) / (_n-_ddof);
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    void fill(const values_type& x, const weights_type&) override { fill(x); }
    /** 
     * Merge another statistics object into this 
     *
     * @param o Other 
     *
     * @return Reference to this 
     */
    welford_var& merge(const welford_var& o)
    {
      assert(o.size() == this->size());
      if (o._n <= 0) return *this;
      if (_n == 0) {
	_n    = o._n;
	_mean = o._mean;
	_var  = o._var;
	return *this;
      }

      real_type   fac  =  real_type(_n-_ddof);
      _n               += o._n;
      real_type   on   =  real_type(o._n);
      values_type dx   =  o._mean - _mean;
      _mean            += value_type(on / _n) * dx;
      values_type dy   =  trait_type::conj(o._mean - _mean);
      _var             *= fac / _n;
      _var             += value_type(on / (_n - _ddof)) * (o._var + dx * dy);
      
      return *this;
    }
    /** @} */

    /** 
     * @{ 
     * @name JSON input/output 
     */
    /** 
     * Merge in from JSON object 
     */
    void merge_json(const json::JSON& json) override
    {
      welford_var<T> w(this->size());
      w.from_json(json);
      merge(w);
    }
    /** @} */
  protected:
    // Clarify scope
    using base::_n;
    // Clarify scope
    using base::_ddof;
    // Clarify scope
    using base::_mean;
    // Clarify scope
    using base::_var;

    std::string id() const override { return "welford_var"; }
  };
  /** 
   * @example welford_var.cc
   * @ingroup statest_stat
   *
   * Example of calculating the variance from an un-weighted sample. 
   *
   * Here, we generate a sample of 100 entries, each consisting of 4
   * random numbers each drawn from a normal distribution.
   *
   * @f[ x_i \sim \mathcal{N}[0,1]\quad\text{for}\quad i=1,\ldots,4\quad.@f]
   *
   */
}
#endif
//
// EOF
//

