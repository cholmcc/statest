/**
 * @file   statest/basic_stat.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Generators of particle data. 
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_BASIC_STAT_HH
#define STAT_BASIC_STAT_HH
#include <statest/trait.hh>
#include <json/json.hh>

/** 
 * @defgroup statest Statistical tools 
 *
 * This library of header files consists of various statistical tools 
 *
 * * @ref statest_stat Tools for calculating means and variances or
 *   covariances on the fly.  These tools provide a robust mechanisms
 *   for calculating sample means and variances or covariances, as
 *   well as merging such statistics from different samples. 
 *
 * * @ref statest_uncer Tools for estimating the uncertainty
 *   (variance) of an estimator of a statistic.  Normal error
 *   propagation relying on derivatives (analytic or numerical) as
 *   well as approximate methods (bootstrap or jackknife) are
 *   implemented.
 *
 */
/** 
 * @defgroup statest_stat Statistics 
 * @ingroup statest 
 *
 * On-line calculations of means, variances, and covariances with or
 * without (component, non-frequency) weights.
 *
 * The classes in this module provides mechanisms for calculating the
 * sample mean, variance, and covariance _on-line_ - that is, we can
 * update the sample mean, variance, and covariance on the fly as data
 * "comes in".  This means we can calcualculate the sample
 * (co)variance in a _single_ pass over the data. 
 *
 * @section stat_1d Single value samples 
 *
 * For a sample 
 *
 * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
 *
 * the mean is given by 
 *
 * @f[\overline{x} = \frac{1}{N}\sum_{n=1}^N x_n\quad,@f]
 *
 * and the variance by
 *
 * @f[
 \mathrm{var}(x) = C\frac{1}{N}\sum_{n=1}^N\left(x_n-\overline{x}\right)^2
 \quad,
 @f] 
 *
 * where @f$ C=N/(N-\delta)@f$ is Bessel's correction (typically
 * @f$\delta=1@f$ for the unbiased estimator).
 *
 * Using these definitions it is obvious that we would need to passes
 * over data to calculate the variance
 *
 * - One pass to calculate the mean @f$\overline{x}@f$ by summing all
 *   observations @f$x_n@f$ and then dividing by the number of observations. 
 *
 * - One pass to calculate the variance summing over the square residuals
 *   @f[\left(x_n-\overline{x}\right)^2@f] 
 *   with respect to the previously calculated mean. 
 *
 * For small samples this is indeed doable, but for larger samples we
 * may want to get rid of the data when we no longer need it - i.e.,
 * we would prefer a single pass over the data.
 *
 * One can show that the variance can be written as 
 *
 * @f[
 \mathrm{var}(x) = C\left(\overline{x^2}-\overline{x}^2\right)\quad,@f]
 *
 * which can be computed in a single pass by summing over all
 * observations and the square observations seperately.  However, this
 * formula, when implemented numerically suffers from _catastrophic
 * cancellation_ (see also @ref catastrophic.cc ).  That is, if the
 * numbers are very large, then the reduced precision by which the
 * computer can represent numbers means that we will introduce and
 * error by calcuting the variance as above.
 *
 * Fortunately, Welford found an algorithm that allows us to
 * continuously update the variance in a single pass over data.  This
 * algorithm is implemented in the class statest::welford_var.  
 *
 * Denoting the mean and variance after @f$n-1@f$ observations
 * @f$\overline{x}_{n-1}@f$ and @f$s^2_{n-1}@f$, respectively, then
 * the after @f$n@f$ observations we have
 *
 @f{align*}{
  \overline{x}_n &= \overline{x}_{n-1} + \frac{1}{n}(x_n-\overline{x}_{n-1}) \\
  s^2_{n}      &= \frac{n-1-\delta}{n}s^2_{n-1} + \frac{1}{n-\delta}(x_n-\overline{x}_{n-1}) (x_n - \overline{x}_n)\\
  \delta         &= n - \begin{cases} 0 & \text{biased}\\ 1 \end{cases}\quad.
  @f}
 * 
 * @section stat_nd Multi-value samples 
 *
 * For a sample 
 *
 * @f[X=\{x_1,\ldots,x_N\}\quad,@f]
 *
 * where each observation @f$x_n@f$ consist of multiple (@f$
 * M@f$) values, we can straightforwardly define the mean as
 *
 * @f[\overline{x} = \frac1N\sum_{n=1}^N x_n\quad.@f] 
 *
 * That is, if each observation is a vector with @f$M@f$ components,
 * then the mean is itself a vector of the same size.
 *
 * Analogous to the variance defined above, we can define the
 * _covariance_ between the @f$ M@f$ components as
 *
 * @f[\mathrm{cov}(x) =
 C\frac1N\sum_{n=1}^N\left(x_n-\overline{x}\right)
 \left(x_n-\overline{x}\right)^T\quad,@f]
 *
 * that is the _outer product_ of the vector residuals with respect to
 * the mean with it self (super-script @f${}^T@f$ means transpose, or
 * more generally conjugate).  The covariance is therefor a square,
 * symmetric matrix of size @f$ M\times M@f$.  Conventionally, this
 * formula is written for components @f$ i,j@f$ as
 *
 * @f[c_{ij} = C\frac{1}{N}\sum_{n=1}
 \left(x_{i,n}-\overline{x_i}\right)
 \left(x_{j,n}-\overline{x_j}\right)\quad.@f]
 *
 * Analogous to the variance above one can show that 
 *
 * @f[\mathrm{cov}(x) = C\left(
 \overline{xx^T}-\overline{x}\overline{x}^T\right)
 \quad,@f] 
 *
 * which however suffers from the same catastrophic cancellation as above. 
 * 
 * One can generalise Welford's algorithm from above to the
 * multi-value case and find that the mean (@f$\overline{x}@f$) and
 * coveriance (@f$c@f$) after @f$ n@f$ observations are given by
 *
 * 
 @f{align*}{
 \overline{x}_n &= \overline{x}_{n-1} + \frac{1}n (x_n - \overline{x}_{n-1})\\
 c_n            &= \frac{n-1-\delta}{n}c_{n-1} + \frac{1}{n-\delta}(x_n - \overline{x}_{n-1}) (x - \overline{x}_n)^T\\
 \delta         &= \begin{cases} 0 & \text{biased}\\ 1 \end{cases}\quad.
 @f}
 *
 * This is implemented in statest::welford_cov. 
 *
 * @section stat_w Weighted samples 
 *
 * West has, for a weighted sample 
 *
 * @f[X=\left\{(x_1,w_1),\ldots,(x_N,w_N)\right\}\quad,@f] 
 *
 * where each observation @f$ x_n@f$ (single or multi-valued) has an
 * associated weight @f$ w_n@f$ (again, single or multi-valued),
 * formulated similar algorithms for calculating the sample mean and
 * (co)variance.  Note that we distinguish between _frequency_ and
 * _non-frequency_ weights.  Frequency weights typically count number
 * of occurrences (i.e., integer valued), while non-frequency weights
 * typically reflect efficiencies and the like.
 *
 * The algorithms are 
 *
 * 
 @f{align*}{
  W_{1,n}        &= W_{1,n-1} + w\\
  W_{2,n}        &= W_{2,n-1} + w^2\\ 
  \overline{x}_n &= \overline{x}_{n-1} + \frac{w}{W_{1,n}} (x - \overline{x}_{n-1})\\
  s^2_{x,n}      &= \frac{\Delta_{n-1}}{\Delta_{n}} s^2_{x,n-1}  + \frac{w}{\Delta_{n}} (x - \overline{x}_{n-1}) (x - \overline{x}_n)\\
  \Delta_n       &= W_{1,n} -  \begin{cases}
                      0 & \text{biased, frequency weights}\\
                      1 & \text{frequency weights}\\
                      \frac{W_{2,n}}{W_{1,n}}\\
                    \end{cases}\quad,
 @f}
 * 
 * for the sample mean and variance, and 
 *
 * 
 @f{align*}{
  \omega  &= \begin{cases} w & \text{observation weights}\\ ww^T & \text{component weights}\end{cases}\\
  W_{1,n}        &= W_{1,n-1} + \omega\\
  W_{2,n}        &= W_{2,n-1} + \omega^2\\
  W_n            &= W_{1,n-1} + w\\
  \overline{x}_n &= \overline{x}_{n-1} + \frac{w}{W_n}(x - \overline{x}_{n-1}) \\
  c_{n}          &= \frac{\Delta_{n-1}}{\Delta_{n}}c_{n-1} + \frac{\omega}{\Delta_n}(x - \overline{x}_{n-1})(x - \overline x_n)^T\\
  \Delta_n       &= W_{1,n} - 
                    \begin{cases}
                      0 & \text{biased frequency weights}\\
                      1 & \text{frequency weights}\\
                      \frac{W_{2,n}}{W_{1,n}}
                    \end{cases}\quad.
 @f}
 *
 * Note, for the multi-valued case, we distinguish between
 * _observation weights_ where @f$ w_n@f$ is single valued and apply
 * to all components of the observation @f$ x_n@f$, or _component
 * weights_ where @f$ w_n@f$ has as many components as the observation
 * @f$ x_n@f$.
 *
 * These two algorithms are implemented in the classes
 * statest::west_var and statest::west_cov.
 */
/** 
 * Namespace for statistics code.
 * 
 * @ingroup statest_stat
 */
namespace statest
{
  //====================================================================
  /** 
   * Base class for statistics with online updates 
   *
   * @ingroup statest_stat 
   * @headerfile "" <statest/basic_stat.hh>
   */
  template <typename T=double>
  struct basic_stat 
  {
    /** Trait type */
    using trait_type=statest::trait<T>;
    /** Value type */
    using value_type=typename trait_type::value_type;
    /** Vector real type */
    using values_type=typename trait_type::values_type;
    /** weight type */
    using weight_type=typename trait_type::real_type;
    /** Vector real type */
    using weights_type=typename trait_type::reals_type;
    /** real type */
    using real_type=typename trait_type::real_type;
    /** Vector real type */
    using reals_type=typename trait_type::reals_type;

    /** Destructor */
    virtual ~basic_stat() {}
    /** 
     * @name Manipulation 
     * @{ 
     */
    /** Reset state */
    virtual void reset()
    {
      std::fill(std::begin(_mean),std::end(_mean),0);
      _n = 0;
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     * @param w  weights 
     */
    virtual void fill(const values_type& x, const weights_type& w) = 0;
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    virtual void fill(const values_type& x) = 0;
    /** @} */

    /** 
     * @name Get information  
     * @{
     */
    /**
     * Get size of statistics
     *
     * @return size of statistics
     */
    size_t size() const { return _mean.size(); }

    /**
     * Get the variances
     *
     * @return the variances
     */
    virtual reals_type variance() const = 0;
  
    /**
     * Get the mean
     *
     * @return the mean
     */
    const values_type& mean() const { return _mean; }

    /**
     * Get the standard deviation
     *
     * @return the standard deviation
     */
    reals_type std() const  { return std::sqrt(variance());  }

    /**
     * Get the standard mean uncertainty
     *
     * @return the standard mean uncertainty
     */
    virtual reals_type sem() const  {  return std() / sqrt(_n); }

    /**
     * Get the number of fills
     *
     * @return the number of fills
     */
    size_t count() const { return _n; }

    /**
     * Get the bias correction for number-degrees-of-freedom 
     *
     * @return @f$\Delta_{\nu}@f$ bias correction for @f$\nu@f$ - the
     * number of degrees of freedom
     */
    size_t ddof() const { return _ddof; }
    /** @} */

    /** 
     * @name Print state 
     * @{ 
     */
    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags
     *
     * @return @a o 
     */
    virtual std::ostream& print(std::ostream& o, unsigned int f=0x1) const = 0;
    /** @} */

    /** 
     * @name Merging statistics objects 
     * @{ 
     */
    /** 
     * Merge other statistics into this.
     *
     * Note, this is not implemented here 
     *
     * @tparam T1 Type of other statistcs 
     * @param o Other statistics 
     *
     * @return Reference to this 
     */
    template <typename T1> basic_stat<T1>& merge(const T1& o);
    /** @} */

    /** 
     * @name JSON input/output 
     * @{ 
     */
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    virtual json::JSON to_json() const
    {
      json::JSON json = {"size",  size(),
			 "ddof",  _ddof,
			 "n",     _n,
			 "means", to_json_array(_mean) };
      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    virtual void from_json(const json::JSON& json)
    {
      size_t s = json["size"].toInt();
      _ddof    = json["ddof"].toInt();
      _n       = json["n"].toInt();
      _mean.resize(s);
      from_json_array(json["means"],_mean);
    }
    /** 
     * Merge from a JSON object 
     *
     * @param json JSON object to merge from 
     */
    virtual void merge_json(const json::JSON& json) = 0;
    /** @} */
  protected:
    /** 
     * Constructor 
     *
     * @param n  size of statistics 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction for @f$\nu@f$ -
     * the number of degress of freedom.
     */
    basic_stat(size_t n, size_t ddof=0)
      : _mean(n),
	_n(0),
	_ddof(ddof)
    {}
    /** Move constructor */
    basic_stat(basic_stat&& o)
      : _mean(std::move(o._mean)),
	_n(o._n),
	_ddof(o._ddof)
    {}
    /** Deleted copy constructor */
    basic_stat(const basic_stat&) = delete;
    /** Deleted assignment operator */
    basic_stat& operator=(const basic_stat&) = delete;

    /** 
     * Make JSON array from array (non-complex case)
     *
     * @tparam C Containter type
     * 
     * - @a c Array to convert 
     *
     * @return JSON array 
     */
    template <typename C,
	      typename std::enable_if<!std::is_same<typename C::value_type,
						    std::complex<double>>::value,
				      int>::type = 0>
    json::JSON to_json_array(const C& c) const
    {
      return json::JSON(c);
    }
    
    /** 
     * Assign content of JSON array to array (non-complex case)
     *
     *
     * @tparam C Containter type
     *
     * - @a json JSON array to read rom 
     * - @a c    Array to assign to 
     */
    template <typename C,
	      typename std::enable_if<!std::is_same<typename C::value_type,
						    std::complex<double>>::value,
				      int>::type = 0>
    void from_json_array(const json::JSON& json, C& c)
    {
      json.toArray(c);
    }

    /** 
     * Make JSON array from array (complex case).  The complex array
     * is flattened to an array of single values. 
     *
     * @tparam C Containter type
     *
     * - @a c Array to convert 
     *
     * @return JSON array 
     */
    template <typename C,
	      typename std::enable_if<std::is_same<typename C::value_type,
						   std::complex<double>>::value,
				      int>::type = 0>
    json::JSON to_json_array(const C& c) const
    {
      reals_type r(2*c.size());
      for (size_t i = 0; i < c.size(); i++) {
	r[2*i+0] = c[i].real();
	r[2*i+1] = c[i].imag();
      }
      return json::JSON(r);
    }

    /** 
     * Assign content of JSON array to array (complex case).  The
     * complex array is assumed to be flattened.
     *
     * @tparam C Containter type
     *
     * - @a json JSON array to read rom 
     * - @a c    Array to assign to 
     */
    template <typename C,
	      typename std::enable_if<std::is_same<typename C::value_type,
						   std::complex<double>>::value,
				      int>::type = 0>
    void from_json_array(const json::JSON& json, C& c)
    {
      reals_type r;
      json.toArray(r);

      c.resize(r.size()/2);
      for (size_t i = 0; i < c.size(); i++) 
	c[i] = std::complex<double>(r[2*i+0],r[2*i+1]);
    }
    

    virtual std::string id() const { return ""; }
    /** Means */
    values_type _mean;
    /** Number of fills */
    size_t _n;
    /** @f$ \Delta_{\nu}@f$ the bias correction for @f$\nu@f$ */
    size_t _ddof;
  };
  /** 
   * @example test_stat.hh 
   * @ingroup statest_stat
   *
   * Header for tests 
   */
  /** 
   * @example test_stat.cc 
   * @ingroup statest_stat
   *
   * @test Test of statistics classes 
   */
}
#endif
//
// EOF
//
