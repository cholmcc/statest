/**
 * @file   statest/basic_cov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Online calculations of mean and covariance 
 */
/*
 * Estimator utilities
 * Copyright (C) 2013 C.H. Christensen <cholmcc@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_COV_HH
#define CORRELATIONS_STAT_COV_HH
#include <statest/basic_stat.hh>

namespace statest
{
  //====================================================================
  /** 
   * Base class for online calculation means and covariance 
   *
   * Implementations of this interface will calculate the mean and
   * covariance of a multi-valued sample (possibly weighted).
   *
   * @tparam T The value type 
   *
   * @ingroup statest_stat 
   * @headerfile "" <statest/basic_cov.hh>
   */
  template <typename T=double>
  struct basic_cov : public basic_stat<T>
  {
    /** base type */
    using base=basic_stat<T>;
    /** trait type */
    using trait_type=typename base::trait_type;
    /** value type */
    using value_type=typename base::value_type;
    /** value vector type */
    using values_type=typename base::values_type;
    /** weight type */
    using weight_type=typename base::weight_type;
    /** weight vector type */
    using weights_type=typename base::weights_type;
    /** real type */
    using real_type=typename base::real_type;
    /** real vector type */
    using reals_type=typename base::reals_type;

    /** Destructor */
    virtual ~basic_cov() {}

    /** Reset the statistics. */
    virtual void reset() override
    {
      base::reset();
      std::fill(std::begin(_cov), std::end(_cov), 0);
    }
    /** 
     * @name Get information
     * @{ 
     */
    // Clarify scope 
    using base::size;

    // Claify scope 
    using base::sem;

    // Claify scope
    using base::std;

    /** @return the covariance */
    virtual const values_type& covariance() const { return _cov; }

    /** @return the variance */
    reals_type variance() const override
    {
      return trait_type::real(_cov[std::slice(0,size(),size()+1)]);
    }

    /** @return the correlation */
    values_type correlation() const
    {
      // reals_type var = variance();
      // reals_type den = std::sqrt(trait_type::outer(var,var));
      reals_type sd  = std();
      reals_type den = trait_type::outer(sd,sd);
      return trait_type::div(covariance(),den);
    }
    /** @} */

    /** 
     * @name Print state
     * @{ 
     */
    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags 
     *
     * @return @a o 
     */
    std::ostream& print(std::ostream& o=std::cout,
			unsigned int  f=0x1) const override
    {
      size_t w = o.width(0);
      o << "n=" << _n << "\t(ddof=" << _ddof << ")\n";

      if (f < 1) return o;
      w = 10;
      
      size_t n = _mean.size();
      auto e = sem();
      values_type c = (f & 0x4 ? correlation() : covariance());
      
      o << "     | "
	<< std::setw(w) << "mean" << " | "
	<< std::setw(w) << "uncer.";
      
      for (size_t i = 0; i < n; i++)  o << " | " << std::setw(w) << i;

      o << "\n-----";
      for (size_t i = 0; i < n+2; i++) {
	o << "+";
	for (size_t j = 0; j < w+2; j++) o << '-';
      }
      o << '\n';
      for (size_t i = 0; i < n; i++) {
	o << std::setw(4) << i        << " | "
	  << std::setw(w) << _mean[i] << " | "
	  << std::setw(w) << e[i];
	for (size_t j = 0; j < n; j++)
	  o << " | " << std::setw(w) << c[i*n+j];
	o << std::endl;
      }

      return o;
    }
    /** @} */

    /** 
     * @name JSON input/output 
     * @{
     */
    // Clarify scope 
    using base::to_json_array;
    // Clarify scope 
    using base::from_json_array;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON to_json() const override
    {
      json::JSON json = base::to_json();
      json["cov"] = to_json_array(_cov);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    void from_json(const json::JSON& json) override
    {
      base::from_json(json);
      from_json_array(json["cov"],_cov);
    }
    /** @} */
  protected:
    /** claify scope */
    using base::_mean;
    /** claify scope */
    using base::_n;
    /** claify scope */
    using base::_ddof;
    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    basic_cov(size_t n,size_t ddof=0)
      : base(n, ddof),
	_cov(n*n)
    {}
    basic_cov(basic_cov&& o) : base(std::move(o)), _cov(std::move(o._cov)) {}

    /** Covariance */
    values_type _cov;
  };
}

#endif 
//
// EOF
//
