# Statistical tools 

This project contains some tools for doing statistics.  Mainly it
deals with estimating the expectation value (mean) and uncertainty on 
that from numerical samples. 

See [examples](examples/) directory for example of use. 

## Cloning 

This package uses the submodules
[json](https://gitlab.com/cholmcc/simplejson),
[options](https://gitlab.com/cholmcc/options) and 
[progress](https://gitlab.com/cholmcc/progress) so when you clone this
pass the option `--recurse-submodules` - i.e., 

```
git clone --recurse-submodules ...
```

If you do not do that, or you forget, then you can populate the
submodules with 

```
git submodule init
git submodule update
```

